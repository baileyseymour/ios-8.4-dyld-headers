/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:29 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Messages/PlugIns/SMS.imservice/SMS
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSString;

@interface SMSAttachmentPart : NSObject {

	NSString* _contentLocation;

}

@property (nonatomic,retain,readonly) NSString * contentLocation;              //@synthesize contentLocation=_contentLocation - In the implementation block
-(void)dealloc;
-(id)initWithContentLocation:(id)arg1 ;
-(NSString *)contentLocation;
@end

