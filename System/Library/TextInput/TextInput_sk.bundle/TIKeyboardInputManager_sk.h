/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:35 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/TextInput/TextInput_sk.bundle/TextInput_sk
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <TextInput_sk/TextInput_sk-Structs.h>
#import <libTextInputCore.dylib/TIKeyboardInputManagerZephyr.h>

@interface TIKeyboardInputManager_sk : TIKeyboardInputManagerZephyr
-(TIInputManagerZephyr*)initImplementation;
-(BOOL)doesComposeText;
-(id)internalStringToExternal:(id)arg1 ;
-(id)externalStringToInternal:(id)arg1 ;
-(id)nonstopPunctuationCharacters;
-(BOOL)shouldAddModifierSymbolsToWordCharacters;
-(id)keyboardConfigurationAccentKeyString;
@end

