/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:34 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/TextInput/TextInput_de.bundle/TextInput_de
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <TextInput_de/TextInput_de-Structs.h>
#import <libTextInputCore.dylib/TIKeyboardInputManagerZephyr.h>

@interface TIKeyboardInputManager_de : TIKeyboardInputManagerZephyr
-(TIInputManagerZephyr*)initImplementation;
-(void)syncToLayoutState:(id)arg1 ;
@end

