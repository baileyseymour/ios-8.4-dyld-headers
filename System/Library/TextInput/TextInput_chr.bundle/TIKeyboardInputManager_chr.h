/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:34 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/TextInput/TextInput_chr.bundle/TextInput_chr
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <TextInput_chr/TextInput_chr-Structs.h>
#import <libTextInputCore.dylib/TIKeyboardInputManagerZephyr.h>

@interface TIKeyboardInputManager_chr : TIKeyboardInputManagerZephyr
-(TIInputManagerZephyr*)initImplementation;
-(BOOL)doesComposeText;
-(id)internalStringToExternal:(id)arg1 ;
-(id)externalStringToInternal:(id)arg1 ;
-(BOOL)dictionaryUsesExternalEncoding;
@end

