/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:36 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/TextInput/libTextInputCore.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <libTextInputCore.dylib/libTextInputCore.dylib-Structs.h>
@class NSString;

@interface TICompiledCollatorFile : NSObject {

	int m_descriptor;
	char* m_mappedFile;
	long long m_length;
	NSString* m_path;

}
+(id)sharedCollatorFile;
+(UCollatorRef)createCollatorWithType:(int)arg1 ;
-(void)dealloc;
-(id)initWithPath:(id)arg1 error:(unsigned*)arg2 ;
-(UCollatorRef)createCollatorWithType:(int)arg1 ;
@end

