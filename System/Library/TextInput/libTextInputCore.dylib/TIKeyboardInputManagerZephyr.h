/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:36 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/TextInput/libTextInputCore.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <libTextInputCore.dylib/libTextInputCore.dylib-Structs.h>
#import <libTextInputCore.dylib/TIKeyboardInputManager.h>
#import <libTextInputCore.dylib/TIRevisionHistoryDelegate.h>

@class NSMutableString, TIKeyboardInputManagerConfig, CPLRUDictionary, TITextCheckerExemptions, NSCharacterSet, TIRevisionHistory, TIAutoshiftRegularExpressionLoader, NSString;

@interface TIKeyboardInputManagerZephyr : TIKeyboardInputManager <TIRevisionHistoryDelegate> {

	TIInputManagerZephyr* m_impl;
	NSMutableString* m_composedText;
	unsigned long long m_initialSelectedIndex;
	shared_ptr<KB::TypologyLogger>* m_typology_recorder;
	BOOL _isEditingWordPrefix;
	BOOL _wordLearningEnabled;
	TIKeyboardInputManagerConfig* _config;
	CPLRUDictionary* _autocorrectionHistory;
	CPLRUDictionary* _rejectedAutocorrections;
	CPLRUDictionary* _autocorrectionsSuggestedForCurrentInput;
	TITextCheckerExemptions* _textCheckerExemptions;
	USetRef _acceptableCharacterSet;
	NSCharacterSet* _allowPredictionCharacterSet;
	TIRevisionHistory* _revisionHistory;
	TIAutoshiftRegularExpressionLoader* _autoshiftRegexLoader;

}

@property (nonatomic,readonly) TIKeyboardInputManagerConfig * config;                                  //@synthesize config=_config - In the implementation block
@property (nonatomic,readonly) TIRevisionHistory * revisionHistory;                                    //@synthesize revisionHistory=_revisionHistory - In the implementation block
@property (nonatomic,readonly) CPLRUDictionary * autocorrectionHistory;                                //@synthesize autocorrectionHistory=_autocorrectionHistory - In the implementation block
@property (nonatomic,readonly) CPLRUDictionary * rejectedAutocorrections;                              //@synthesize rejectedAutocorrections=_rejectedAutocorrections - In the implementation block
@property (nonatomic,readonly) CPLRUDictionary * autocorrectionsSuggestedForCurrentInput;              //@synthesize autocorrectionsSuggestedForCurrentInput=_autocorrectionsSuggestedForCurrentInput - In the implementation block
@property (assign,getter=isWordLearningEnabled,nonatomic) BOOL wordLearningEnabled;                    //@synthesize wordLearningEnabled=_wordLearningEnabled - In the implementation block
@property (assign,nonatomic) BOOL isEditingWordPrefix;                                                 //@synthesize isEditingWordPrefix=_isEditingWordPrefix - In the implementation block
@property (nonatomic,retain) TIAutoshiftRegularExpressionLoader * autoshiftRegexLoader;                //@synthesize autoshiftRegexLoader=_autoshiftRegexLoader - In the implementation block
@property (nonatomic,readonly) TITextCheckerExemptions * textCheckerExemptions;                        //@synthesize textCheckerExemptions=_textCheckerExemptions - In the implementation block
@property (nonatomic,readonly) USetRef acceptableCharacterSet;                                         //@synthesize acceptableCharacterSet=_acceptableCharacterSet - In the implementation block
@property (nonatomic,readonly) NSCharacterSet * allowPredictionCharacterSet;                           //@synthesize allowPredictionCharacterSet=_allowPredictionCharacterSet - In the implementation block
@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
-(id)humanReadableTrace;
-(void)clearHumanReadableTrace;
-(void)storeLanguageModelDynamicDataIncludingCache;
-(BOOL)isTypologyEnabled;
-(void)logToTypologyRecorderWithString:(id)arg1 ;
-(id)configurationPropertyList;
-(void)setInput:(id)arg1 ;
-(void)dealloc;
-(void)suspend;
-(BOOL)usesCandidateSelection;
-(id)candidateResultSet;
-(unsigned long long)initialSelectedIndex;
-(id)autocorrection;
-(void)clearInput;
-(void)setWordLearningEnabled:(BOOL)arg1 ;
-(unsigned)inputCount;
-(id)inputString;
-(void)textAccepted:(id)arg1 ;
-(unsigned)inputIndex;
-(void)resume;
-(BOOL)nextInputWouldStartSentence;
-(id)shadowTyping;
-(void)setOriginalInput:(id)arg1 ;
-(id)defaultCandidate;
-(void)setInHardwareKeyboardMode:(BOOL)arg1 ;
-(void)candidateRejected:(id)arg1 ;
-(id)autocorrectionRecordForWord:(id)arg1 ;
-(void)setKeyboardEventsLagging:(BOOL)arg1 ;
-(BOOL)canHandleKeyHitTest;
-(BOOL)needsKeyHitTestResults;
-(long long)keyHitTest:(id)arg1 ;
-(id)autocorrectionList;
-(TIKeyboardInputManagerConfig *)config;
-(id)initWithInputMode:(id)arg1 ;
-(void)incrementUnigramCount:(id)arg1 usageFlags:(unsigned)arg2 ;
-(void)incrementLanguageModelCount:(unsigned)arg1 context:(const unsigned*)arg2 contextLength:(unsigned long long)arg3 ;
-(void)decrementUnigramCount:(id)arg1 usageFlags:(unsigned)arg2 ;
-(void)decrementLanguageModelCount:(unsigned)arg1 context:(const unsigned*)arg2 contextLength:(unsigned long long)arg3 ;
-(unsigned)tokenIDForWord:(id)arg1 context:(const unsigned*)arg2 contextLength:(unsigned long long)arg3 createIfNecessary:(BOOL)arg4 ;
-(TIInputManagerZephyr*)initImplementation;
-(BOOL)usesRetrocorrection;
-(void)loadDictionaries;
-(BOOL)doesComposeText;
-(void)deltaPathsChanged:(id)arg1 ;
-(id)initWithConfig:(id)arg1 ;
-(TIRevisionHistory *)revisionHistory;
-(TITextCheckerExemptions *)textCheckerExemptions;
-(void)setCollatorLocale;
-(void)syncToKeyboardState:(id)arg1 afterContextChange:(BOOL)arg2 ;
-(BOOL)updateLanguageModelForKeyboardState;
-(void)loadFavoniusLanguageModel;
-(void)installTypologyTraceLogger;
-(void)refreshInputManagerState;
-(id)internalStringToExternal:(id)arg1 ;
-(id)externalStringToInternal:(id)arg1 ;
-(BOOL)dictionaryUsesExternalEncoding;
-(id)pathToDeltaStaticDictionary;
-(void)updateComposedText;
-(void)setAutoshiftFromInputContext;
-(unsigned)simulateAutoshiftIfNecessaryForFlags:(unsigned)arg1 ;
-(void)dropInput;
-(void)dropInputPrefix:(unsigned)arg1 ;
-(BOOL)hasLegacyInputStem;
-(BOOL)canStartSentenceAfterString:(id)arg1 ;
-(BOOL)acceptsRange:(NSRange)arg1 inString:(id)arg2 ;
-(NSRange)acceptableRangeFromRange:(NSRange)arg1 inText:(id)arg2 withSelectionLocation:(unsigned long long)arg3 ;
-(NSRange)legacyInputRangeForTokenRange:(NSRange)arg1 ;
-(NSRange)inputStringRangeFromRevisionHistory;
-(void)setInputIndex:(unsigned)arg1 ;
-(void)updateInputContext;
-(BOOL)canComputeSentenceContextForInputStem:(id)arg1 ;
-(BOOL)shouldAllowContextTokenID:(unsigned)arg1 ;
-(id)inputStem;
-(unsigned)externalIndexToInternal:(unsigned)arg1 ;
-(id)trimmedInputStem;
-(unsigned long long)internalIndexOfInputStemSuffix:(id)arg1 ;
-(BOOL)canRetrocorrectInputAtIndex:(unsigned)arg1 ;
-(BOOL)canTrimInputAtIndex:(unsigned)arg1 ;
-(BOOL)shouldDropInputStem;
-(CPLRUDictionary *)autocorrectionsSuggestedForCurrentInput;
-(void)checkAutocorrectionDictionaries;
-(void)updateForRevisitedString:(id)arg1 ;
-(LanguageModelContext*)sentenceContextForInputStem:(id)arg1 ;
-(unsigned)internalIndexToExternal:(unsigned)arg1 ;
-(void)recordAcceptedAutocorrection:(id)arg1 ;
-(void)recordRejectedAutocorrectionForAcceptedText:(id)arg1 ;
-(BOOL)shouldAllowCorrectionOfAcceptedCandidate:(id)arg1 ;
-(BOOL)isWordLearningEnabled;
-(BOOL)shouldSuppressLearning;
-(id)dictionaryStringForExternalString:(id)arg1 ;
-(void)acceptInput;
-(CPLRUDictionary *)autocorrectionHistory;
-(CPLRUDictionary *)rejectedAutocorrections;
-(id)nonstopPunctuationCharacters;
-(BOOL)nextInputWouldStartSentenceAfterInput:(id)arg1 ;
-(TIAutoshiftRegularExpressionLoader *)autoshiftRegexLoader;
-(id)sentenceDelimitingCharacters;
-(id)sentenceTrailingCharacters;
-(id)sentencePrefixingCharacters;
-(void)setAutoshiftRegexLoader:(TIAutoshiftRegularExpressionLoader *)arg1 ;
-(id)inputContext;
-(unsigned long long)prefixLengthOfInput:(id)arg1 sharedWithCandidate:(const Candidate*)arg2 ;
-(BOOL)isEditingExistingWord;
-(id)phraseCandidateCompletedByWord:(const String*)arg1 allowNoSuggest:(BOOL)arg2 ;
-(id)autocorrectionCandidateForInput:(id)arg1 withCandidate:(const Candidate*)arg2 ;
-(void)recordSuggestedAutocorrection:(id)arg1 ;
-(id)autocorrectionCandidateForInput:(id)arg1 withCandidate:(const Candidate*)arg2 insertingSpace:(BOOL)arg3 sharedPrefixLength:(unsigned long long)arg4 ;
-(BOOL)shouldSkipShortcutConversionForDocumentState:(id)arg1 ;
-(NSRange)shortcutSearchRangeForString:(id)arg1 ;
-(void)enumerateWordSuffixesOfString:(id)arg1 inRange:(NSRange)arg2 usingBlock:(/*^block*/id)arg3 ;
-(id)shortcutConversionForString:(id)arg1 stringStartsInMiddleOfWord:(BOOL)arg2 ;
-(id)cannedResponsesToString:(id)arg1 ;
-(BOOL)suggestionBlacklistMatchesStrings:(id)arg1 ;
-(BOOL)spaceAndNextInputWouldStartSentence;
-(id)terminatorsPrecedingAutospace;
-(BOOL)shouldInsertSpaceBeforePredictions;
-(BOOL)shouldAutocapitalizePredictionAfterSpace;
-(id)cannedResponseCandidatesForString:(id)arg1 ;
-(id)indexesOfDuplicatesInCandidates:(id)arg1 ;
-(id)predictionCandidates;
-(NSCharacterSet *)allowPredictionCharacterSet;
-(BOOL)shouldGenerateSuggestionsForSelectedText;
-(id)autocorrectionListForSelectedText;
-(BOOL)shouldGeneratePredictionsForCurrentContext;
-(id)autocorrectionListForEmptyInputWithDesiredCandidateCount:(unsigned long long)arg1 ;
-(id)completionCandidates;
-(id)topCandidate;
-(id)extendedAutocorrection:(id)arg1 spanningInputsForCandidates:(id)arg2 ;
-(USetRef)createAcceptableCharacterSet;
-(BOOL)acceptsCharacter:(unsigned)arg1 ;
-(void)setLearnsCorrection:(BOOL)arg1 ;
-(BOOL)inHardwareKeyboardMode;
-(void)clearDynamicDictionary;
-(void)releaseDynamicLanguageModel;
-(id)addInput:(id)arg1 flags:(unsigned)arg2 point:(CGPoint)arg3 firstDelete:(unsigned long long*)arg4 fromVariantKey:(BOOL)arg5 ;
-(BOOL)hasLegacyInputString;
-(void)setInputStringFromDocumentState:(id)arg1 ;
-(void)trimInput;
-(id)deleteFromInput:(unsigned long long*)arg1 ;
-(id)internalInputContext;
-(unsigned)tokenIDForWordSeparator:(unsigned short)arg1 ;
-(BOOL)shouldExpectSentenceBoundaryAfterContext:(const unsigned*)arg1 contextLength:(unsigned long long)arg2 ;
-(BOOL)shouldAddModifierSymbolsToWordCharacters;
-(id)wordCharacters;
-(unsigned long long)prefixLengthOfInput:(id)arg1 sharedWithCandidates:(const vector<KB::Candidate, std::__1::allocator<KB::Candidate> >*)arg2 ;
-(id)terminatorsPreventingAutocorrection;
-(id)terminatorsDeletingAutospace;
-(BOOL)supportsShortcutConversion;
-(unsigned long long)maximumShortcutLengthAllowed;
-(id)shortcutCompletionsForDocumentState:(id)arg1 ;
-(id)shortcutConversionForDocumentState:(id)arg1 ;
-(id)shortcutConversionForInput:(id)arg1 andExistingString:(id)arg2 existingStringStartsInMiddleOfWord:(BOOL)arg3 ;
-(id)candidatesForString:(id)arg1 ;
-(void)reconcileCandidates:(vector<KB::Candidate, std::__1::allocator<KB::Candidate> >*)arg1 forTypedString:(String*)arg2 withPhraseCandidate:(Candidate*)arg3 replacing:(const String*)arg4 ;
-(void)clearKeyAreas;
-(void)registerKeyArea:(CGRect)arg1 keyCode:(short)arg2 keyString:(const char*)arg3 ;
-(BOOL)isEditingWordPrefix;
-(void)setIsEditingWordPrefix:(BOOL)arg1 ;
-(USetRef)acceptableCharacterSet;
@end

