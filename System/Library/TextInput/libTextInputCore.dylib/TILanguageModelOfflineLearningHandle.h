/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:36 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/TextInput/libTextInputCore.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <libTextInputCore.dylib/libTextInputCore.dylib-Structs.h>
@class TIInputMode, NSDate, NSString;

@interface TILanguageModelOfflineLearningHandle : NSObject {

	BOOL _valid;
	TIInputMode* _inputMode;
	NSDate* _lastAdaptationDate;
	NSString* _currentAppContext;
	shared_ptr<KB::StaticDictionary>* _dictionary;
	shared_ptr<KB::LanguageModel>* _currentModel;

}

@property (nonatomic,readonly) TIInputMode * inputMode;                                   //@synthesize inputMode=_inputMode - In the implementation block
@property (nonatomic,readonly) shared_ptr<KB::StaticDictionary>* dictionary;              //@synthesize dictionary=_dictionary - In the implementation block
@property (assign,getter=isValid,nonatomic) BOOL valid;                                   //@synthesize valid=_valid - In the implementation block
@property (nonatomic,copy) NSDate * lastAdaptationDate;                                   //@synthesize lastAdaptationDate=_lastAdaptationDate - In the implementation block
@property (assign,nonatomic) shared_ptr<KB::LanguageModel>* currentModel;                 //@synthesize currentModel=_currentModel - In the implementation block
@property (nonatomic,copy) NSString * currentAppContext;                                  //@synthesize currentAppContext=_currentAppContext - In the implementation block
-(void)dealloc;
-(shared_ptr<KB::StaticDictionary>*)dictionary;
-(BOOL)isValid;
-(TIInputMode *)inputMode;
-(void)setValid:(BOOL)arg1 ;
-(id)initWithInputMode:(id)arg1 ;
-(NSDate *)lastAdaptationDate;
-(void)setLastAdaptationDate:(NSDate *)arg1 ;
-(shared_ptr<KB::LanguageModel>*)currentModel;
-(void)setCurrentModel:(shared_ptr<KB::LanguageModel>*)arg1 ;
-(NSString *)currentAppContext;
-(void)setCurrentAppContext:(NSString *)arg1 ;
@end

