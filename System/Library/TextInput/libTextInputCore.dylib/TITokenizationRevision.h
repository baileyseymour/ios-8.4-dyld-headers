/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:36 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/TextInput/libTextInputCore.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <libTextInputCore.dylib/libTextInputCore.dylib-Structs.h>
@class NSMutableArray;

@interface TITokenizationRevision : NSObject {

	unsigned long long _revisedDocumentLocation;
	unsigned long long _branchedTokenIndex;
	unsigned long long _mergedTokenIndex;
	NSMutableArray* _branchTokens;
	NSRange _originalSelectedTokenRange;
	TIRevisionHistoryTokenIterator _originalIterator;

}

@property (assign,nonatomic) NSRange originalSelectedTokenRange;                           //@synthesize originalSelectedTokenRange=_originalSelectedTokenRange - In the implementation block
@property (assign,nonatomic) TIRevisionHistoryTokenIterator originalIterator;              //@synthesize originalIterator=_originalIterator - In the implementation block
@property (assign,nonatomic) unsigned long long revisedDocumentLocation;                   //@synthesize revisedDocumentLocation=_revisedDocumentLocation - In the implementation block
@property (assign,nonatomic) unsigned long long branchedTokenIndex;                        //@synthesize branchedTokenIndex=_branchedTokenIndex - In the implementation block
@property (assign,nonatomic) unsigned long long mergedTokenIndex;                          //@synthesize mergedTokenIndex=_mergedTokenIndex - In the implementation block
@property (nonatomic,readonly) NSMutableArray * branchTokens;                              //@synthesize branchTokens=_branchTokens - In the implementation block
-(void)dealloc;
-(id)init;
-(id)initWithTokenIterator:(TIRevisionHistoryTokenIterator)arg1 ;
-(NSRange)originalSelectedTokenRange;
-(void)setOriginalSelectedTokenRange:(NSRange)arg1 ;
-(TIRevisionHistoryTokenIterator)originalIterator;
-(void)setOriginalIterator:(TIRevisionHistoryTokenIterator)arg1 ;
-(unsigned long long)revisedDocumentLocation;
-(void)setRevisedDocumentLocation:(unsigned long long)arg1 ;
-(unsigned long long)branchedTokenIndex;
-(void)setBranchedTokenIndex:(unsigned long long)arg1 ;
-(unsigned long long)mergedTokenIndex;
-(void)setMergedTokenIndex:(unsigned long long)arg1 ;
-(NSMutableArray *)branchTokens;
@end

