/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:39 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/MultipeerConnectivity.framework/MultipeerConnectivity
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

typedef struct OpaqueGCKSession* OpaqueGCKSessionRef;

typedef struct OpaqueAGPSession* OpaqueAGPSessionRef;

typedef struct {
	int field1;
	char* field2;
	int field3;
	unsigned field4;
} SCD_Struct_MC2;

typedef struct _NSZone* NSZoneRef;

