/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:13 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/Foundation.framework/Foundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <Foundation/NSError.h>

@interface NSCFError : NSError
+(BOOL)automaticallyNotifiesObserversForKey:(id)arg1 ;
-(id)retain;
-(BOOL)isEqual:(id)arg1 ;
-(unsigned long long)retainCount;
-(unsigned long long)hash;
-(id)domain;
-(long long)code;
-(Class)classForCoder;
-(id)userInfo;
-(BOOL)allowsWeakReference;
-(BOOL)retainWeakReference;
-(oneway void)release;
-(void)finalize;
@end

