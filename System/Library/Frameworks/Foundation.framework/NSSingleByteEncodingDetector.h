/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:17 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/Foundation.framework/Foundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <Foundation/NSEncodingDetector.h>

@interface NSSingleByteEncodingDetector : NSEncodingDetector
-(double)confidence;
@end

