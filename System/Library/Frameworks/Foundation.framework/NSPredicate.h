/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:17 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/Foundation.framework/Foundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <Foundation/Foundation-Structs.h>
#import <Foundation/NSSecureCoding.h>
#import <Foundation/NSCopying.h>

@class NSString;

@interface NSPredicate : NSObject <NSSecureCoding, NSCopying> {

	struct {
		unsigned _evaluationBlocked : 1;
		unsigned _reservedPredicateFlags : 31;
	}  _predicateFlags;
	unsigned reserved;

}

@property (copy,readonly) NSString * predicateFormat; 
+(id)predicateForAssetsInAssetCollectionWithID:(id)arg1 ;
+(id)predicateForCloudInvitationsInAssetCollection:(id)arg1 ;
+(id)predicateForCommentsInAsset:(id)arg1 ;
+(id)predicateForLikesInAsset:(id)arg1 ;
+(CFLocaleRef)retainedLocale;
+(id)predicateWithFormat:(id)arg1 argumentArray:(id)arg2 ;
+(id)newStringFrom:(id)arg1 usingUnicodeTransforms:(unsigned long long)arg2 ;
+(id)predicateWithFormat:(id)arg1 arguments:(char*)arg2 ;
+(void)initialize;
+(BOOL)supportsSecureCoding;
+(id)predicateWithValue:(BOOL)arg1 ;
+(id)predicateWithBlock:(/*^block*/id)arg1 ;
+(id)predicateWithFormat:(id)arg1 ;
-(id)br_watchedURL;
-(void)ab_bindStatement:(CPSqliteStatement*)arg1 withBindingOffset:(int*)arg2 predicateIdentifier:(int)arg3 ;
-(BOOL)ab_hasCallback;
-(id)ab_newQueryWithSortOrder:(unsigned)arg1 addressBook:(void*)arg2 propertyIndices:(const _CFDictionary*)arg3 ;
-(void)ab_addCallbackContextToArray:(CFArrayRef)arg1 ;
-(void)ab_runPredicateWithSortOrder:(unsigned)arg1 inAddressBook:(void*)arg2 withDelegate:(id)arg3 ;
-(void)validate;
-(id)minimalFormInContext:(id)arg1 ;
-(void)_validateForMetadataQueryScopes:(id)arg1 ;
-(id)generateMetadataDescription;
-(void)allowEvaluation;
-(void)acceptVisitor:(id)arg1 flags:(unsigned long long)arg2 ;
-(id)predicateWithSubstitutionVariables:(id)arg1 ;
-(NSString *)predicateFormat;
-(BOOL)_allowsEvaluation;
-(BOOL)evaluateWithObject:(id)arg1 substitutionVariables:(id)arg2 ;
-(id)initWithCoder:(id)arg1 ;
-(void)encodeWithCoder:(id)arg1 ;
-(id)description;
-(id)copyWithZone:(NSZone*)arg1 ;
-(BOOL)evaluateWithObject:(id)arg1 ;
@end

