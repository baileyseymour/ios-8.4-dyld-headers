/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:20 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/Foundation.framework/Foundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <Foundation/Foundation-Structs.h>
@class NSLock, NSMutableSet, NSMutableDictionary;

@interface __NSBundleTables : NSObject {

	NSLock* _lock;
	NSMutableSet* _staticFrameworks;
	NSMutableSet* _loadedBundles;
	NSMutableSet* _loadedFrameworks;
	NSMutableDictionary* _resolvedPathToBundles;
	CFDictionaryRef _bundleForClassMap;

}
+(id)bundleTables;
-(id)allBundles;
-(id)allFrameworks;
-(void)removeBundle:(id)arg1 forPath:(id)arg2 type:(unsigned long long)arg3 ;
-(void)setBundle:(id)arg1 forClass:(Class)arg2 ;
-(id)loadedBundles;
-(void)addStaticFrameworkBundles:(id)arg1 ;
-(id)bundleForPath:(id)arg1 ;
-(void)addBundle:(id)arg1 type:(unsigned long long)arg2 ;
-(id)addBundle:(id)arg1 forPath:(id)arg2 ;
-(void)dealloc;
-(id)init;
-(id)bundleForClass:(Class)arg1 ;
-(void)finalize;
@end

