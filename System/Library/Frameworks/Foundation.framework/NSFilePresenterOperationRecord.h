/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:19 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/Foundation.framework/Foundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSString;

@interface NSFilePresenterOperationRecord : NSObject {

	NSString* operationDescription;
	long long state;
	id reactor;

}

@property (readonly) NSString * operationDescription; 
@property (readonly) long long state; 
@property (assign) id reactor; 
+(id)operationRecordWithDescription:(id)arg1 ;
-(void)setReactor:(id)arg1 ;
-(void)didBegin;
-(void)willEnd;
-(void)didEnd;
-(id)reactor;
-(NSString *)operationDescription;
-(void)dealloc;
-(id)description;
-(long long)state;
@end

