/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:20 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/Foundation.framework/Foundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <Foundation/Foundation-Structs.h>
#import <Foundation/NSCoder.h>

@protocol NSObject;
@interface NSXPCCoder : NSCoder {

	id<NSObject> _userInfo;
	id _reserved1;

}

@property (retain) id<NSObject> userInfo;              //@synthesize userInfo=_userInfo - In the implementation block
-(void)dealloc;
-(id)init;
-(void)setUserInfo:(id<NSObject>)arg1 ;
-(id<NSObject>)userInfo;
-(void)encodeXPCObject:(id)arg1 forKey:(id)arg2 ;
-(id)decodeXPCObjectForKey:(id)arg1 ;
-(id)decodeXPCObjectOfType:(xpc_type_sRef)arg1 forKey:(id)arg2 ;
-(BOOL)requiresSecureCoding;
@end

