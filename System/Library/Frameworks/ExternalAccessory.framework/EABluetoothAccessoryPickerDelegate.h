/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:12 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/ExternalAccessory.framework/ExternalAccessory
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol EABluetoothAccessoryPickerDelegate <NSObject>
@optional
-(void)devicePicker:(id)arg1 didSelectAddress:(id)arg2 errorCode:(long long)arg3;

@end

