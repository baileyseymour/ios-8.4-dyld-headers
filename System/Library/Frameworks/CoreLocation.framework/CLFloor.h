/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:04 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/CoreLocation.framework/CoreLocation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <CoreLocation/CoreLocation-Structs.h>
#import <CoreLocation/NSCopying.h>
#import <CoreLocation/NSSecureCoding.h>

@interface CLFloor : NSObject <NSCopying, NSSecureCoding> {

	long long level;

}

@property (nonatomic,readonly) long long level; 
+(BOOL)supportsSecureCoding;
-(id)initWithCoder:(id)arg1 ;
-(void)encodeWithCoder:(id)arg1 ;
-(id)copyWithZone:(NSZone*)arg1 ;
-(long long)level;
-(id)initWithLevel:(long long)arg1 ;
@end

