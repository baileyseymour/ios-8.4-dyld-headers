/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:38 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/Metal.framework/Metal
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

@class NSDictionary;


@protocol MTLCommandBufferSPI <MTLCommandBuffer>
@property (getter=isProfilingEnabled) BOOL profilingEnabled; 
@property (readonly) NSDictionary * profilingResults; 
@optional
-(id)sampledRenderCommandEncoderWithDescriptor:(id)arg1 programInfoBuffer:(/*function pointer*/void**)arg2 capacity:(unsigned long long)arg3;
-(id)sampledComputeCommandEncoderWithProgramInfoBuffer:(/*function pointer*/void**)arg1 capacity:(unsigned long long)arg2;
-(id)debugCommandEncoder;

@required
-(BOOL)isProfilingEnabled;
-(void)setProfilingEnabled:(BOOL)arg1;
-(NSDictionary *)profilingResults;

@end

