/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:27 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/WebKit.framework/WebKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <WebKit/WKDOMNode.h>

@class NSString;

@interface WKDOMText : WKDOMNode

@property (nonatomic,copy) NSString * data; 
-(NSString *)data;
-(void)setData:(NSString *)arg1 ;
@end

