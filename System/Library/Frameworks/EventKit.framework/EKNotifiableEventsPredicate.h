/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:06 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/EventKit.framework/EventKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <EventKit/EventKit-Structs.h>
#import <Foundation/NSPredicate.h>
#import <EventKit/EKDefaultPropertiesLoading.h>
#import <EventKit/NSSecureCoding.h>

@class NSString;

@interface EKNotifiableEventsPredicate : NSPredicate <EKDefaultPropertiesLoading, NSSecureCoding>

@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
+(BOOL)supportsSecureCoding;
+(id)predicate;
-(id)defaultPropertiesToLoad;
-(BOOL)shouldLoadDefaultProperties;
-(id)copyWithZone:(NSZone*)arg1 ;
-(BOOL)evaluateWithObject:(id)arg1 ;
@end

