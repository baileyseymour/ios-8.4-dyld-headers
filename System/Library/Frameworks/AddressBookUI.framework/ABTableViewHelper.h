/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:48 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/AddressBookUI.framework/AddressBookUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface ABTableViewHelper : NSObject
+(void)syncTableView:(id)arg1 withIndexPath:(id)arg2 animated:(BOOL)arg3 styleProvider:(id)arg4 ;
+(void)prepareCell:(id)arg1 asSelectedCell:(BOOL)arg2 styleProvider:(id)arg3 ;
+(id)newOverlayLabel;
@end

