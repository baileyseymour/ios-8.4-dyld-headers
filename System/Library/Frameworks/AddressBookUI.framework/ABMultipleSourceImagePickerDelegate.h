/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:44 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/AddressBookUI.framework/AddressBookUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol ABMultipleSourceImagePickerDelegate
@required
-(void)multipleSourcePicker:(id)arg1 didSelectPerson:(id)arg2;
-(void)multipleSourcePickerDidSelectPhotoPicker:(id)arg1;

@end

