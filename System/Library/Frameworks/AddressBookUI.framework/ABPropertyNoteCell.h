/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:47 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/AddressBookUI.framework/AddressBookUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <AddressBookUI/ABPropertyCell.h>

@class UITextView, UILabel;

@interface ABPropertyNoteCell : ABPropertyCell {

	UITextView* _textView;
	UILabel* _labelLabel;

}

@property (nonatomic,retain) UITextView * textView;               //@synthesize textView=_textView - In the implementation block
@property (nonatomic,readonly) UILabel * labelLabel;              //@synthesize labelLabel=_labelLabel - In the implementation block
-(void)performDefaultAction;
-(void)setContactStyle:(id)arg1 ;
-(void)setLabelTextAttributes:(id)arg1 ;
-(id)labelView;
-(void)setValueTextAttributes:(id)arg1 ;
-(double)bottomBaselineConstant;
-(void)textViewChanged:(id)arg1 ;
-(void)textViewEditingDidEnd:(id)arg1 ;
-(id)valueView;
-(void)dealloc;
-(id)initWithStyle:(long long)arg1 reuseIdentifier:(id)arg2 ;
-(void)setTextView:(UITextView *)arg1 ;
-(void)setAllowsEditing:(BOOL)arg1 ;
-(UITextView *)textView;
-(UILabel *)labelLabel;
-(BOOL)shouldPerformDefaultAction;
@end

