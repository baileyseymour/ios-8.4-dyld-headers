/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:38 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/AVFoundation.framework/AVFoundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <AVFoundation/AVFoundation-Structs.h>
@class AVCaptureSession_FigRecorder, AVCaptureConnection_FigRecorder, CALayer, NSString;

@interface AVCaptureVideoPreviewLayerInternal_FigRecorder : NSObject {

	AVCaptureSession_FigRecorder* session;
	AVCaptureConnection_FigRecorder* connection;
	CALayer* sublayer;
	CGSize sensorSize;
	NSString* sensorToPreviewVTScalingMode;
	CGSize previewSize;
	double previewRotationDegrees;
	NSString* gravity;
	BOOL disableActions;
	long long orientation;
	BOOL automaticallyAdjustsMirroring;
	BOOL mirrored;
	BOOL isPresentationLayer;
	BOOL visible;
	BOOL isPaused;
	BOOL chromaNoiseReductionEnabled;
	int changeSeed;
	CGAffineTransform captureDeviceTransform;
	double rollAdjustment;

}
@end

