/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:40 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/AVFoundation.framework/libAVFAudio.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSURL, NSData, NSDictionary, NSArray;

@interface AVAudioPlayer : NSObject {

	id _impl;

}

@property (getter=isPlaying,readonly) BOOL playing; 
@property (readonly) unsigned long long numberOfChannels; 
@property (readonly) double duration; 
@property (assign) id<AVAudioPlayerDelegate> delegate; 
@property (readonly) NSURL * url; 
@property (readonly) NSData * data; 
@property (assign) float pan; 
@property (assign) float volume; 
@property (assign) BOOL enableRate; 
@property (assign) float rate; 
@property (assign) double currentTime; 
@property (readonly) double deviceCurrentTime; 
@property (assign) long long numberOfLoops; 
@property (readonly) NSDictionary * settings; 
@property (getter=isMeteringEnabled) BOOL meteringEnabled; 
@property (nonatomic,copy) NSArray * channelAssignments; 
-(id)impl;
-(id)initBase;
-(void)finishedPlaying:(id)arg1 ;
-(void)decodeError:(id)arg1 ;
-(void)beginInterruption;
-(void)endInterruptionWithFlags:(id)arg1 ;
-(void)endInterruption;
-(float)pan;
-(void)setPan:(float)arg1 ;
-(BOOL)isMeteringEnabled;
-(float)peakPowerForChannel:(unsigned long long)arg1 ;
-(id)initWithData:(id)arg1 fileTypeHint:(id)arg2 error:(id*)arg3 ;
-(id)initWithContentsOfURL:(id)arg1 fileTypeHint:(id)arg2 error:(id*)arg3 ;
-(BOOL)playAtTime:(double)arg1 ;
-(void)setEnableRate:(BOOL)arg1 ;
-(BOOL)enableRate;
-(double)deviceCurrentTime;
-(void)setNumberOfLoops:(long long)arg1 ;
-(long long)numberOfLoops;
-(NSArray *)channelAssignments;
-(void)setChannelAssignments:(NSArray *)arg1 ;
-(BOOL)mixToUplink;
-(void)setMixToUplink:(BOOL)arg1 ;
-(BOOL)prepareToPlay;
-(BOOL)play;
-(void)setCurrentTime:(double)arg1 ;
-(void)dealloc;
-(void)setDelegate:(id<AVAudioPlayerDelegate>)arg1 ;
-(id<AVAudioPlayerDelegate>)delegate;
-(double)duration;
-(NSURL *)url;
-(NSDictionary *)settings;
-(void)stop;
-(NSData *)data;
-(void)pause;
-(void)setMeteringEnabled:(BOOL)arg1 ;
-(void)updateMeters;
-(float)averagePowerForChannel:(unsigned long long)arg1 ;
-(id)initWithContentsOfURL:(id)arg1 error:(id*)arg2 ;
-(unsigned long long)numberOfChannels;
-(float)rate;
-(void)setRate:(float)arg1 ;
-(id)initWithData:(id)arg1 error:(id*)arg2 ;
-(void)finalize;
-(float)volume;
-(void)setVolume:(float)arg1 ;
-(double)currentTime;
-(BOOL)isPlaying;
@end

