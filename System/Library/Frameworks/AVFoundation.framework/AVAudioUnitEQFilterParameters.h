/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:40 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/AVFoundation.framework/libAVFAudio.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <libAVFAudio.dylib/libAVFAudio.dylib-Structs.h>
@interface AVAudioUnitEQFilterParameters : NSObject {

	void* _impl;

}

@property (assign,nonatomic) long long filterType; 
@property (assign,nonatomic) float frequency; 
@property (assign,nonatomic) float bandwidth; 
@property (assign,nonatomic) float gain; 
@property (assign,nonatomic) BOOL bypass; 
-(id)initWithImpl:(AVAudioUnitEQFilterParametersImpl*)arg1 ;
-(void)setBandwidth:(float)arg1 ;
-(float)bandwidth;
-(void)setBypass:(BOOL)arg1 ;
-(BOOL)bypass;
-(float)gain;
-(void)setGain:(float)arg1 ;
-(void)dealloc;
-(id)init;
-(float)frequency;
-(void)setFrequency:(float)arg1 ;
-(long long)filterType;
-(void)setFilterType:(long long)arg1 ;
@end

