/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:41 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/AVFoundation.framework/libAVFAudio.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSURL, AVAudioFormat;

@interface AVAudioFile : NSObject {

	void* _impl;

}

@property (nonatomic,readonly) NSURL * url; 
@property (nonatomic,readonly) AVAudioFormat * fileFormat; 
@property (nonatomic,readonly) AVAudioFormat * processingFormat; 
@property (nonatomic,readonly) long long length; 
@property (assign,nonatomic) long long framePosition; 
-(id)initSecondaryReader:(id)arg1 format:(id)arg2 error:(id*)arg3 ;
-(void)setFramePosition:(long long)arg1 ;
-(BOOL)readIntoBuffer:(id)arg1 frameCount:(unsigned)arg2 error:(id*)arg3 ;
-(long long)framePosition;
-(id)initForReading:(id)arg1 commonFormat:(unsigned long long)arg2 interleaved:(BOOL)arg3 error:(id*)arg4 ;
-(id)initForWriting:(id)arg1 settings:(id)arg2 commonFormat:(unsigned long long)arg3 interleaved:(BOOL)arg4 error:(id*)arg5 ;
-(id)initForReading:(id)arg1 error:(id*)arg2 ;
-(id)initForWriting:(id)arg1 settings:(id)arg2 error:(id*)arg3 ;
-(BOOL)readIntoBuffer:(id)arg1 error:(id*)arg2 ;
-(BOOL)writeFromBuffer:(id)arg1 error:(id*)arg2 ;
-(AVAudioFormat *)processingFormat;
-(AVAudioFormat *)fileFormat;
-(void)dealloc;
-(long long)length;
-(NSURL *)url;
@end

