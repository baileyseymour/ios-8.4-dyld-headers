/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:41 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/AVFoundation.framework/libAVFAudio.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <libAVFAudio.dylib/libAVFAudio.dylib-Structs.h>
@class NSArray;

@interface AVAudioSessionRouteDescription : NSObject {

	void* _impl;

}

@property (readonly) NSArray * inputs; 
@property (readonly) NSArray * outputs; 
+(id)privateCreateOrConfigure:(id)arg1 withRawDescription:(id)arg2 ;
-(NSArray *)outputs;
-(NSArray *)inputs;
-(RouteDescriptionImpl*)privateGetImplementation;
-(BOOL)isEqualToRoute:(id)arg1 ;
-(BOOL)matchesRawDescription:(id)arg1 ;
-(void)dealloc;
-(id)init;
-(BOOL)isEqual:(id)arg1 ;
-(id)description;
@end

