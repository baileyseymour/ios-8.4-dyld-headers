/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:40 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/AVFoundation.framework/libAVFAudio.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <libAVFAudio.dylib/libAVFAudio.dylib-Structs.h>
@class NSURL, NSDictionary, NSArray;

@interface AVAudioRecorder : NSObject {

	void* _impl;

}

@property (getter=isRecording,readonly) BOOL recording; 
@property (readonly) NSURL * url; 
@property (readonly) NSDictionary * settings; 
@property (assign) id<AVAudioRecorderDelegate> delegate; 
@property (readonly) double currentTime; 
@property (readonly) double deviceCurrentTime; 
@property (getter=isMeteringEnabled) BOOL meteringEnabled; 
@property (nonatomic,copy) NSArray * channelAssignments; 
-(BOOL)isRecording;
-(AudioRecorderImpl*)impl;
-(void)finishedRecording;
-(BOOL)isMeteringEnabled;
-(float)peakPowerForChannel:(unsigned long long)arg1 ;
-(double)deviceCurrentTime;
-(NSArray *)channelAssignments;
-(void)setChannelAssignments:(NSArray *)arg1 ;
-(void)privCommonCleanup;
-(id)initWithURL:(id)arg1 settings:(id)arg2 error:(id*)arg3 ;
-(BOOL)prepareToRecord;
-(BOOL)recordAtTime:(double)arg1 ;
-(BOOL)recordForDuration:(double)arg1 ;
-(BOOL)recordAtTime:(double)arg1 forDuration:(double)arg2 ;
-(BOOL)deleteRecording;
-(void)endInterruptionWithFlags;
-(void)beginInterruption;
-(void)endInterruption;
-(void)dealloc;
-(void)setDelegate:(id<AVAudioRecorderDelegate>)arg1 ;
-(id<AVAudioRecorderDelegate>)delegate;
-(NSURL *)url;
-(NSDictionary *)settings;
-(void)stop;
-(void)pause;
-(id)baseInit;
-(void)setMeteringEnabled:(BOOL)arg1 ;
-(void)updateMeters;
-(float)averagePowerForChannel:(unsigned long long)arg1 ;
-(BOOL)record;
-(void)finalize;
-(double)currentTime;
@end

