/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:01 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/CoreImage.framework/CoreImage
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <CoreImage/_CICompositeFilter.h>

@interface CIAdditionCompositing : _CICompositeFilter
+(id)_filter_display_name;
+(id)_kernel_name;
+(id)_kernel_source;
-(id)outputImage;
@end

