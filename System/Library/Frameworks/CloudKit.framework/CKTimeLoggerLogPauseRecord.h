/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:51 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/CloudKit.framework/CloudKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSString;

@interface CKTimeLoggerLogPauseRecord : NSObject {

	double _time;
	NSString* _message;

}

@property (assign,nonatomic) double time;                     //@synthesize time=_time - In the implementation block
@property (nonatomic,retain) NSString * message;              //@synthesize message=_message - In the implementation block
-(NSString *)message;
-(void)setTime:(double)arg1 ;
-(double)time;
-(void)setMessage:(NSString *)arg1 ;
@end

