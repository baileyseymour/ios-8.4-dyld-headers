/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:07 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

@class UIScreen, NSDictionary;


@protocol _UIScreenBasedObject <NSObject>
@property (readonly) UIScreen * _intendedScreen; 
@property (readonly) NSDictionary * _options; 
@required
-(NSDictionary *)_options;
-(id)_initWithScreen:(id)arg1 options:(id)arg2;
-(BOOL)_matchingOptions:(id)arg1;
-(UIScreen *)_intendedScreen;

@end

