/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:05 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class UIRemoveControl, UIControl, UIView;

@interface _UITableViewCellOldEditingData : NSObject {

	UIRemoveControl* _removeControl;
	UIControl* _grabber;
	UIView* _separator;

}

@property (nonatomic,retain) UIControl * reorderControl;              //@synthesize grabber=_grabber - In the implementation block
@property (nonatomic,retain) UIView * separatorView;                  //@synthesize separator=_separator - In the implementation block
-(void)dealloc;
-(BOOL)dataRequired;
-(UIControl *)reorderControl;
-(void)setReorderControl:(UIControl *)arg1 ;
-(UIView *)separatorView;
-(void)setSeparatorView:(UIView *)arg1 ;
@end

