/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:55 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIKit-Structs.h>
#import <UIKit/UIView.h>

@class UIView;

@interface UIKBEmojiSnapshotSizingView : UIView {

	CGSize _snapshotSize;
	UIView* _snapshotView;

}

@property (nonatomic,readonly) UIView * snapshotView;              //@synthesize snapshotView=_snapshotView - In the implementation block
-(void)dealloc;
-(void)layoutSubviews;
-(id)initWithSnapshotView:(id)arg1 ;
-(UIView *)snapshotView;
@end

