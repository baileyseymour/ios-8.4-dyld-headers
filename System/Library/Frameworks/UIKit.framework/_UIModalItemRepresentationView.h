/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:00 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIView.h>

@class UIView;

@interface _UIModalItemRepresentationView : UIView {

	BOOL _useFakeEffectSource;
	UIView* _fakeEffectSourceView;

}

@property (assign,nonatomic) BOOL useFakeEffectSource;              //@synthesize useFakeEffectSource=_useFakeEffectSource - In the implementation block
-(void)layoutSubviews;
-(void)setUseFakeEffectSource:(BOOL)arg1 animated:(BOOL)arg2 ;
-(void)setUseFakeEffectSource:(BOOL)arg1 ;
-(BOOL)useFakeEffectSource;
@end

