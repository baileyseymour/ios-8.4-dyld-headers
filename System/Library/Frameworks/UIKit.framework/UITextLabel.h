/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:03 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIKit-Structs.h>
#import <UIKit/UILabel.h>

@interface UITextLabel : UILabel
-(void)setAlignment:(long long)arg1 ;
-(void)drawRect:(CGRect)arg1 ;
-(long long)alignment;
-(id)color;
-(void)setColor:(id)arg1 ;
-(void)setCentersHorizontally:(BOOL)arg1 ;
-(BOOL)centersHorizontally;
-(BOOL)wrapsText;
-(CGSize)ellipsizedTextSize;
-(void)setHighlightedColor:(id)arg1 ;
-(id)highlightedColor;
-(void)setWrapsText:(BOOL)arg1 ;
-(void)setTextAutoresizesToFit:(BOOL)arg1 ;
-(BOOL)textAutoresizesToFit;
-(void)setMinFontSize:(float)arg1 ;
-(float)minFontSize;
@end

