/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:09 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIAlertControllerVisualStyleAlert.h>

@interface UIAlertControllerVisualStyleAlertCar : UIAlertControllerVisualStyleAlert
-(double)maximumWidth;
-(id)backgroundView;
-(double)verticalContentMargin;
-(id)titleLabelFont;
-(id)titleLabelColor;
-(long long)maximumNumberOfLinesInTitleLabel;
-(id)messageLabelFont;
-(id)messageLabelColor;
-(long long)maximumNumberOfLinesInMessageLabel;
-(double)horizontalContentMargin;
-(double)visualAltitude;
-(id)regularActionFont;
-(id)defaultActionFont;
-(id)actionHighlightedBackgroundView;
-(id)highlightedActionContentColor;
-(double)minimumActionHeight;
@end

