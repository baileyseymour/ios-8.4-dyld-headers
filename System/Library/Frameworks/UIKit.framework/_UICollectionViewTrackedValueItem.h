/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:18 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface _UICollectionViewTrackedValueItem : NSObject {

	double _timeStamp;
	double _value;

}

@property (assign,nonatomic) double timeStamp;              //@synthesize timeStamp=_timeStamp - In the implementation block
@property (assign,nonatomic) double value;                  //@synthesize value=_value - In the implementation block
-(double)value;
-(void)setValue:(double)arg1 ;
-(double)timeStamp;
-(void)setTimeStamp:(double)arg1 ;
@end

