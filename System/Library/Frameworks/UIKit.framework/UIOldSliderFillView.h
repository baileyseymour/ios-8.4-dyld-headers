/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:06 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIKit-Structs.h>
#import <UIKit/UIView.h>

@class UIImage;

@interface UIOldSliderFillView : UIView {

	UIImage* _sliderImage;

}
-(void)drawRect:(CGRect)arg1 ;
-(id)initWithFrame:(CGRect)arg1 image:(id)arg2 ;
@end

