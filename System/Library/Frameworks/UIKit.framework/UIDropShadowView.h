/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:12 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIKit-Structs.h>
#import <UIKit/UIView.h>

@class UIView, NSDictionary;

@interface UIDropShadowView : UIView {

	double _cornerRadius;
	UIView* _contentView;
	UIView* _backgroundImage;
	NSDictionary* _preservedLayerValues;

}

@property (assign) double cornerRadius;                         //@synthesize cornerRadius=_cornerRadius - In the implementation block
@property (assign,nonatomic) UIView * contentView;              //@synthesize contentView=_contentView - In the implementation block
-(void)dealloc;
-(id)init;
-(void)setFrame:(CGRect)arg1 ;
-(void)setBounds:(CGRect)arg1 ;
-(UIView *)contentView;
-(void)setContentView:(UIView *)arg1 ;
-(double)cornerRadius;
-(void)setCornerRadius:(double)arg1 ;
-(void)willBeginRotationWithOriginalBounds:(CGRect)arg1 newBounds:(CGRect)arg2 ;
-(void)didFinishRotation;
-(void)updateShadowPath;
@end

