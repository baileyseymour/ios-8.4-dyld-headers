/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:58 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface _UIViewAnimationDelegate : NSObject {

	/*^block*/id _completionBlock;

}
-(void)dealloc;
-(void)animationDidStop:(id)arg1 finished:(BOOL)arg2 ;
-(id)initWithCompletionBlock:(/*^block*/id)arg1 ;
@end

