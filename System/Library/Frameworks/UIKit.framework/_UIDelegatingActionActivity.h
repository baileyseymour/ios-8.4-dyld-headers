/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:12 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIActivity.h>

@class UIDocumentInteractionController;

@interface _UIDelegatingActionActivity : UIActivity {

	UIDocumentInteractionController* _documentInteractionController;
	SEL _action;

}
-(id)activityType;
-(id)_activityImage;
-(id)activityTitle;
-(BOOL)canPerformWithActivityItems:(id)arg1 ;
-(void)performActivity;
-(id)initWithDocumentInteractionController:(id)arg1 forAction:(SEL)arg2 ;
@end

