/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:23 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIKit-Structs.h>
#import <UIKit/UIView.h>

@interface _UIWebFindOnPageHighlightBubbleView : UIView {

	CGImageRef _highlightedContent;
	CGPoint _highlightedContentOrigin;

}
-(void)dealloc;
-(void)drawRect:(CGRect)arg1 ;
-(void)setHighlightedContent:(CGImageRef)arg1 withOrigin:(CGPoint)arg2 ;
@end

