/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:23 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <QuartzCore/CATransformLayer.h>

@interface _UIFlippingLayer : CATransformLayer
-(void)setOpaque:(BOOL)arg1 ;
@end

