/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:04 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIKit-Structs.h>
#import <UIKit/UIView.h>

@class NSNotification, NSLayoutConstraint;

@interface _UIKeyboardLayoutAlignmentView : UIView {

	NSNotification* _keyboardChangeNotificationForUpdateConstraints;
	CGRect lastKnownKeyboardRect;
	NSLayoutConstraint* bottomConstraint;
	NSLayoutConstraint* widthConstraint;
	NSLayoutConstraint* heightConstraint;

}
-(id)initWithFrame:(CGRect)arg1 ;
-(void)dealloc;
-(void)didMoveToWindow;
-(void)willMoveToWindow:(id)arg1 ;
-(void)updateConstraints;
-(CGRect)_frameInBoundsForKeyboardFrame:(CGRect)arg1 ;
-(void)_updateConstraintsToMatchKeyboardFrame:(CGRect)arg1 ;
-(void)_keyboardChanged:(id)arg1 ;
-(void)_removeConstraints;
-(void)_addConstraints;
@end

