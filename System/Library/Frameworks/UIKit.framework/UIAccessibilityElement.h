/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:09 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIKit-Structs.h>
#import <UIKit/UIAccessibilityIdentification.h>

@class NSString, NSArray;

@interface UIAccessibilityElement : NSObject <UIAccessibilityIdentification>

@property (assign,nonatomic) id accessibilityDelegate; 
@property (assign,nonatomic) CGRect bounds; 
@property (nonatomic,retain) NSArray * accessibilityContainerElements; 
@property (assign,nonatomic) id accessibilityContainer; 
@property (assign,nonatomic) BOOL isAccessibilityElement; 
@property (nonatomic,retain) NSString * accessibilityLabel; 
@property (nonatomic,retain) NSString * accessibilityHint; 
@property (nonatomic,retain) NSString * accessibilityValue; 
@property (assign,nonatomic) CGRect accessibilityFrame; 
@property (assign,nonatomic) unsigned long long accessibilityTraits; 
@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
@property (nonatomic,copy) NSString * accessibilityIdentifier; 
-(BOOL)representsSubview;
-(BOOL)elementMatchesSubview:(id)arg1 ;
-(double)initialYOffset;
-(void)storeSubviewData:(id)arg1 ;
-(void)setTableCellYOffset:(float)arg1 ;
-(void)setAccessibilityDelegate:(id)arg1 ;
-(NSArray *)accessibilityContainerElements;
-(void)setTableCellYOffset:(float)arg1 ;
-(CGRect)bounds;
-(void)setBounds:(CGRect)arg1 ;
-(void)setData:(id)arg1 forKey:(id)arg2 ;
-(id)dataForKey:(id)arg1 ;
-(id)accessibilityDelegate;
-(void)setDelegate:(id)arg1 forAttribute:(int)arg2 withSelector:(SEL)arg3 ;
-(void)delegateSpecificsForAttribute:(int)arg1 delegate:(id*)arg2 selector:(SEL*)arg3 ;
-(void)setAccessibilityContainerElements:(NSArray *)arg1 ;
-(id)initWithAccessibilityContainer:(id)arg1 ;
@end

