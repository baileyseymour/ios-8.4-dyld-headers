/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:04 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class UIResponder;

@interface _UITouchForwardingRecipient : NSObject {

	UIResponder* fromResponder;
	UIResponder* responder;
	long long recordedPhase;
	long long autocompletedPhase;

}

@property (assign,nonatomic) UIResponder * fromResponder; 
@property (assign,nonatomic) UIResponder * responder; 
@property (assign,nonatomic) long long recordedPhase; 
@property (assign,nonatomic) long long autocompletedPhase; 
-(id)description;
-(void)setResponder:(UIResponder *)arg1 ;
-(UIResponder *)responder;
-(long long)recordedPhase;
-(long long)autocompletedPhase;
-(void)setRecordedPhase:(long long)arg1 ;
-(void)setAutocompletedPhase:(long long)arg1 ;
-(id)initWithResponder:(id)arg1 fromResponder:(id)arg2 ;
-(UIResponder *)fromResponder;
-(void)setFromResponder:(UIResponder *)arg1 ;
@end

