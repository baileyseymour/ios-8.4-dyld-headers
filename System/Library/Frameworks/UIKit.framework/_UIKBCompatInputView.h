/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:17 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIKit-Structs.h>
#import <UIKit/UIView.h>

@class UIView;

@interface _UIKBCompatInputView : UIView

@property (nonatomic,readonly) UIView * touchableView; 
+(BOOL)_retroactivelyRequiresConstraintBasedLayout;
-(id)hitTest:(CGPoint)arg1 withEvent:(id)arg2 ;
-(BOOL)pointInside:(CGPoint)arg1 withEvent:(id)arg2 ;
-(void)setBounds:(CGRect)arg1 ;
-(CGSize)intrinsicContentSize;
-(void)_resizeForKeyplaneSize:(CGSize)arg1 splitWidthsChanged:(BOOL)arg2 ;
-(CGRect)_compatibleBounds;
-(BOOL)_hasAutolayoutHeightConstraint;
-(UIView *)touchableView;
@end

