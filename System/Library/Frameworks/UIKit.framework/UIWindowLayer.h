/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:56 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/UIKit.framework/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIKit-Structs.h>
#import <QuartzCore/CALayer.h>

@class UIWindow;

@interface UIWindowLayer : CALayer {

	UIWindow* _window;

}
-(void)setFrame:(CGRect)arg1 ;
-(void)setBounds:(CGRect)arg1 ;
-(void)setTransform:(CATransform3D)arg1 ;
-(id)actionForKey:(id)arg1 ;
@end

