/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:00 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/CoreFoundation.framework/CoreFoundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <CoreFoundation/CoreFoundation-Structs.h>
#import <CoreFoundation/CFPDDataBuffer.h>

@interface CFPDPurgeableBuffer : CFPDDataBuffer {

	CFDataRef handle;
	unsigned long long allocSize;
	BOOL safe;
	BOOL usedMalloc;

}
-(BOOL)beginAccessing;
-(void)endAccessing;
-(id)initWithPropertyList:(void*)arg1 ;
-(void)dealloc;
-(unsigned long long)length;
-(void*)bytes;
@end

