/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:41 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/AVKit.framework/AVKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol NSCoding
@required
-(id)initWithCoder:(id)arg1;
-(void)encodeWithCoder:(id)arg1;

@end

