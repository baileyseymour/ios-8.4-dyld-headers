/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:29 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/MediaPlayer.framework/MediaPlayer
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol MPAVPlaylistManagerDelegate <NSObject>
@required
-(void)playlistManager:(id)arg1 didTransitionToPlaylistFeeder:(id)arg2;
-(void)playlistManager:(id)arg1 didFailLoadingAllItemsForQueueFeeder:(id)arg2;

@end

