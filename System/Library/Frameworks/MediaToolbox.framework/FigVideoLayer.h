/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:34 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/MediaToolbox.framework/MediaToolbox
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <QuartzCore/CALayer.h>

@class FigVideoLayerInternal;

@interface FigVideoLayer : CALayer {

	FigVideoLayerInternal* _videoLayer;

}
-(void)notificationBarrier;
-(BOOL)isVideoLayerBeingServiced;
-(id)layerDisplayName;
-(void)_sendVideoLayerIsBeingServicedNotification;
-(void)layerDidBecomeVisible:(BOOL)arg1 ;
-(void)dealloc;
-(id)init;
-(id)initWithLayer:(id)arg1 ;
-(void)finalize;
@end

