/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:46 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/QuartzCore.framework/QuartzCore
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

@class NSDictionary;


@protocol EAGLDrawable
@property (copy) NSDictionary * drawableProperties; 
@required
-(NSDictionary *)drawableProperties;
-(void)setDrawableProperties:(id)arg1;

@end

