/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:50 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/CFNetwork.framework/CFNetwork
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <CFNetwork/CFNetwork-Structs.h>
@class NSString;

@interface NSURLCacheInternal : NSObject {

	unsigned long long memoryCapacity;
	unsigned long long diskCapacity;
	NSString* diskPath;
	unsigned long long currentMemoryUsage;
	unsigned long long currentDiskUsage;
	CFURLCache* _cacheRef;

}
-(void)dealloc;
-(void)finalize;
@end

