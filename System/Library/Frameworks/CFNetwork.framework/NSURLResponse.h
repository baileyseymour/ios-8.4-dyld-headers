/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:49 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/CFNetwork.framework/CFNetwork
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <CFNetwork/CFNetwork-Structs.h>
#import <CFNetwork/NSSecureCoding.h>
#import <CFNetwork/NSCopying.h>

@class NSURLResponseInternal, NSURL, NSString;

@interface NSURLResponse : NSObject <NSSecureCoding, NSCopying> {

	NSURLResponseInternal* _internal;

}

@property (nonatomic,readonly) BOOL ssv_isExpiredResponse; 
@property (copy,readonly) NSURL * URL; 
@property (copy,readonly) NSString * MIMEType; 
@property (readonly) long long expectedContentLength; 
@property (copy,readonly) NSString * textEncodingName; 
@property (copy,readonly) NSString * suggestedFilename; 
+(id)_responseWithCFURLResponse:(CFURLResponseRef)arg1 ;
+(id)getObjectKeyWithIndex:(long long)arg1 ;
+(BOOL)supportsSecureCoding;
-(id)_cacheTime;
-(int)statusCode;
-(id)allHeaderFields;
-(long long)maxExpectedContentLength;
-(id)copyXPCEncoding;
-(id)initWithXPCEncoding:(id)arg1 ;
-(BOOL)ssv_isExpiredResponse;
-(void)_setExpectedContentLength:(long long)arg1 ;
-(void)_setMIMEType:(id)arg1 ;
-(id)_lastModifiedDate;
-(BOOL)_mustRevalidate;
-(double)_freshnessLifetime;
-(double)_calculatedExpiration;
-(id)_peerCertificateChain;
-(void)dealloc;
-(CFURLResponseRef)_CFURLResponse;
-(id)_initWithCFURLResponse:(CFURLResponseRef)arg1 ;
-(NSString *)textEncodingName;
-(NSString *)suggestedFilename;
-(id)initWithCoder:(id)arg1 ;
-(void)encodeWithCoder:(id)arg1 ;
-(id)init;
-(id)description;
-(id)copyWithZone:(NSZone*)arg1 ;
-(NSURL *)URL;
-(NSString *)MIMEType;
-(long long)expectedContentLength;
-(id)initWithURL:(id)arg1 MIMEType:(id)arg2 expectedContentLength:(long long)arg3 textEncodingName:(id)arg4 ;
@end

