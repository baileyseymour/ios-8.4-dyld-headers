/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:50 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/CFNetwork.framework/CFNetwork
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <CFNetwork/NSURLCredentialStorage.h>

@interface __NSCFMemoryURLCredentialStorage : NSURLCredentialStorage
+(void)initialize;
-(id)allCredentials;
-(void)removeCredential:(id)arg1 forProtectionSpace:(id)arg2 ;
-(id)credentialsForProtectionSpace:(id)arg1 ;
-(void)setCredential:(id)arg1 forProtectionSpace:(id)arg2 ;
-(void)setDefaultCredential:(id)arg1 forProtectionSpace:(id)arg2 ;
-(id)init;
-(id)description;
-(id)defaultCredentialForProtectionSpace:(id)arg1 ;
@end

