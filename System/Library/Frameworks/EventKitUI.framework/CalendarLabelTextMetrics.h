/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:10 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/EventKitUI.framework/EventKitUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

@class UIFont, UIColor, NSString;


@protocol CalendarLabelTextMetrics
@property (assign,nonatomic) long long numberOfLines; 
@property (assign,nonatomic) long long lineBreakMode; 
@property (assign,nonatomic) long long textAlignment; 
@property (nonatomic,retain) UIFont * font; 
@property (nonatomic,retain) UIColor * textColor; 
@property (nonatomic,copy) NSString * text; 
@required
-(void)setNumberOfLines:(long long)arg1;
-(void)setTextAlignment:(long long)arg1;
-(void)setTextColor:(id)arg1;
-(void)setFont:(id)arg1;
-(NSString *)text;
-(void)setText:(id)arg1;
-(void)setLineBreakMode:(long long)arg1;
-(UIFont *)font;
-(UIColor *)textColor;
-(long long)textAlignment;
-(long long)lineBreakMode;
-(CGRect*)textRectForBounds:(CGRect)arg1 limitedToNumberOfLines:(long long)arg2;
-(long long)numberOfLines;

@end

