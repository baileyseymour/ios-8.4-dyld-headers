/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:10 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/EventKitUI.framework/EventKitUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <EventKitUI/EKEventDetailCell.h>

@class UILabel, UITextView;

@interface EKEventDetailNotesCell : EKEventDetailCell {

	UILabel* _notesTitleView;
	UITextView* _notesView;
	BOOL _isTruncatingNotes;

}

@property (nonatomic,readonly) BOOL isTruncatingNotes;              //@synthesize isTruncatingNotes=_isTruncatingNotes - In the implementation block
-(void)layoutForWidth:(double)arg1 position:(int)arg2 ;
-(BOOL)isTruncatingNotes;
-(id)_notesView;
-(id)_notesTitleView;
-(BOOL)update;
@end

