/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:08 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/EventKitUI.framework/EventKitUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <EventKitUI/EKReminderInlineEditItem.h>

@interface EKReminderNotesInlineEditItem : EKReminderInlineEditItem
-(void)addStylingToCell:(id)arg1 forSubitemAtIndex:(unsigned long long)arg2 ;
-(id)newCell;
-(id)textFromReminder;
-(BOOL)saveAndDismissWithForce:(BOOL)arg1 ;
-(BOOL)shouldAppearWithVisibility:(int)arg1 ;
-(long long)cellStyle;
@end

