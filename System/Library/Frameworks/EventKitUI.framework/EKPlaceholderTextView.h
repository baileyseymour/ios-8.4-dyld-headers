/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:09 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/EventKitUI.framework/EventKitUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <EventKitUI/EventKitUI-Structs.h>
#import <UIKit/UITextView.h>

@class UILabel;

@interface EKPlaceholderTextView : UITextView {

	UILabel* _placeholder;
	BOOL _showingPlaceholder;

}
-(void)_updatePlaceholder;
-(id)initWithFrame:(CGRect)arg1 ;
-(void)dealloc;
-(void)layoutSubviews;
-(void)setText:(id)arg1 ;
-(void)setPlaceholder:(id)arg1 ;
-(void)textChanged:(id)arg1 ;
-(id)placeholder;
-(id)_placeholderLabel;
@end

