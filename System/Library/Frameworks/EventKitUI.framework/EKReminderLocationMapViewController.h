/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:10 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/EventKitUI.framework/EventKitUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <EventKitUI/EventKitUI-Structs.h>
#import <UIKit/UIViewController.h>

@class EKStructuredLocation;

@interface EKReminderLocationMapViewController : UIViewController {

	CGRect _mapViewFrame;
	EKStructuredLocation* _location;

}
-(id)initWithFrame:(CGRect)arg1 location:(id)arg2 ;
-(void)loadView;
@end

