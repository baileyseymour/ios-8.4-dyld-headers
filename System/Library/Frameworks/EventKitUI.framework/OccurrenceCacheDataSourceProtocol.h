/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:10 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/EventKitUI.framework/EventKitUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol OccurrenceCacheDataSourceProtocol <NSObject>
@required
-(void)stopSearching;
-(void)searchWithTerm:(id)arg1;
-(BOOL)cachedOccurrencesAreBeingGenerated;
-(BOOL)cachedOccurrencesAreLoaded;
-(long long)cachedDayCount;
-(BOOL)supportsFakeTodaySection;
-(long long)sectionForCachedOccurrencesOnDate:(id)arg1;
-(long long)countOfOccurrencesAtDayIndex:(long long)arg1;
-(id)dateAtDayIndex:(long long)arg1;
-(id)cachedOccurrenceAtIndexPath:(id)arg1;
-(void)fetchDaysInBackgroundStartingFromSection:(long long)arg1;
-(id)initWithEventStore:(id)arg1 calendars:(id)arg2;
-(void)invalidateCachedOccurrences;
-(BOOL)supportsInvitations;
-(void)invalidate;

@end

