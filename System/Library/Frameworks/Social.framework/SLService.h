/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:50 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/Social.framework/Social
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface SLService : NSObject
+(id)allServices;
+(id)serviceForServiceType:(id)arg1 ;
-(void)addExtraParameters:(id)arg1 forRequest:(id)arg2 ;
-(BOOL)isFirstClassService;
-(long long)maximumURLCount;
-(long long)maximumImageCount;
-(long long)maximumImageDataSize;
-(long long)maximumVideoCount;
-(long long)maximumVideoDataSize;
-(long long)maximumVideoTimeLimit;
-(BOOL)supportsImageURL:(id)arg1 ;
-(BOOL)supportsVideoURL:(id)arg1 ;
-(id)composeViewController;
-(id)activityTitle;
-(id)activityImage;
-(id)accountType;
-(id)serviceType;
-(BOOL)hasAccounts;
@end

