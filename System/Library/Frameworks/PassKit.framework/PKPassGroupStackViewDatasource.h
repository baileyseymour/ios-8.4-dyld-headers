/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:42 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/PassKit.framework/PassKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol PKPassGroupStackViewDatasource <NSObject>
@optional
-(id)groupStackView:(id)arg1 headerForPassType:(unsigned long long)arg2;
-(id)groupStackView:(id)arg1 subheaderForPassType:(unsigned long long)arg2;
-(BOOL)groupStackViewShouldShowHeaderViews:(id)arg1;
-(BOOL)groupStackView:(id)arg1 requiresHeaderForPassType:(unsigned long long)arg2;

@required
-(unsigned long long)indexOfGroup:(id)arg1;
-(id)groupAtIndex:(unsigned long long)arg1;
-(unsigned long long)indexOfSeparationGroup;
-(unsigned long long)numberOfGroups;

@end

