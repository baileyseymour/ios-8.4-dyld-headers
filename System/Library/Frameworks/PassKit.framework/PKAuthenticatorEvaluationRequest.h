/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:43 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/PassKit.framework/PassKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSString, NSNumber;

@interface PKAuthenticatorEvaluationRequest : NSObject {

	NSString* _PINTitle;
	NSNumber* _PINLength;
	long long _policy;

}

@property (nonatomic,copy) NSString * PINTitle;               //@synthesize PINTitle=_PINTitle - In the implementation block
@property (nonatomic,copy) NSNumber * PINLength;              //@synthesize PINLength=_PINLength - In the implementation block
@property (nonatomic,readonly) long long policy;              //@synthesize policy=_policy - In the implementation block
-(long long)policy;
-(id)initWithPolicy:(long long)arg1 ;
-(void)setPINTitle:(NSString *)arg1 ;
-(void)setPINLength:(NSNumber *)arg1 ;
-(NSString *)PINTitle;
-(NSNumber *)PINLength;
-(void)dealloc;
-(id)init;
@end

