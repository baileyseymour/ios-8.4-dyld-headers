/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:42 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/PassKit.framework/PassKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol PKPassDeleteDelegate <NSObject>
@optional
-(void)deleteSheet:(id)arg1 didComplete:(BOOL)arg2;

@end

