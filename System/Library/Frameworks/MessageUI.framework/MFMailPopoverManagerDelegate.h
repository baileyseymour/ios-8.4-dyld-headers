/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:01:34 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/MessageUI.framework/MessageUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol MFMailPopoverManagerDelegate <NSObject>
@optional
-(void)popoverControllerDidDismissPopover:(id)arg1 isUserAction:(BOOL)arg2;
-(BOOL)popoverControllerSupportsRotation:(id)arg1;
-(long long)popoverPresentationStyleForViewController:(id)arg1;
-(BOOL)popoverControllerShouldDismissPopover:(id)arg1;

@end

