/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:43 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/Frameworks/AddressBook.framework/AddressBook
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol CNVCardActivityAlertAbstractSerializationStrategy <NSObject>
@required
+(BOOL)strategyWouldAlterString:(id)arg1;
+(id)serializeString:(id)arg1;

@end

