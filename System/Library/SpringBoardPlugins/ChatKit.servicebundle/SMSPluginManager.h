/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:34 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/SpringBoardPlugins/ChatKit.servicebundle/ChatKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <ChatKit/SBPluginBundleController.h>
#import <ChatKit/SBBulletinAlertHandler.h>

@class BBBulletin, NSString;

@interface SMSPluginManager : NSObject <SBPluginBundleController, SBBulletinAlertHandler> {

	BOOL _displayExtraAlerts;
	BBBulletin* _lastBulletin;

}

@property (assign,nonatomic) BOOL displayExtraAlerts;               //@synthesize displayExtraAlerts=_displayExtraAlerts - In the implementation block
@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
+(BOOL)carrierSMSReceiveOnlyEnabled;
+(id)sharedInstance;
+(void)awakeFromBundle;
-(void)dealloc;
-(id)init;
-(void)volumeChanged:(id)arg1 ;
-(void)applicationLaunched:(id)arg1 ;
-(void)setDisplayExtraAlerts:(BOOL)arg1 ;
-(void)postAlertForMessageWithDictionary:(id)arg1 ;
-(BOOL)bindBulletin:(id)arg1 forRegistry:(id)arg2 ;
-(void)handleEvent:(int)arg1 withBulletin:(id)arg2 forRegistry:(id)arg3 ;
-(BOOL)displayExtraAlerts;
@end

