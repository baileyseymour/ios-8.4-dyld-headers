/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:33 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/SearchBundles/VoiceMemos.searchBundle/VoiceMemos
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <VoiceMemos/VoiceMemos-Structs.h>
#import <VoiceMemos/SPSearchDatastore.h>

@class NSString;

@interface SPVoiceMemosDatastore : NSObject <SPSearchDatastore>

@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
-(id)searchDomains;
-(id)resultForIdentifier:(id)arg1 domain:(unsigned)arg2 ;
-(id)displayIdentifierForDomain:(unsigned)arg1 ;
-(void)performQuery:(id)arg1 withResultsPipe:(id)arg2 ;
-(void)_performVMQuery:(id)arg1 withResultsPipe:(id)arg2 ;
@end

