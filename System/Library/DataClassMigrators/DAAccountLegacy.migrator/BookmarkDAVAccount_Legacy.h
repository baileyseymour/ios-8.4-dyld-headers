/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:33 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/DataClassMigrators/DAAccountLegacy.migrator/DAAccountLegacy
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <DAAccountLegacy/DALegacyAccount.h>

@interface BookmarkDAVAccount_Legacy : DALegacyAccount
-(BOOL)upgradeAccountWithParent:(id)arg1 ;
-(id)_oldURLsForKeychain;
-(int)bookmarkDAVAccountVersion;
@end

