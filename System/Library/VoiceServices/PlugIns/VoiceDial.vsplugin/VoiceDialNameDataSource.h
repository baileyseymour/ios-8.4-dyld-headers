/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:37 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/VoiceServices/PlugIns/VoiceDial.vsplugin/VoiceDial
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface VoiceDialNameDataSource : NSObject {

	unsigned long long _nameTypeMask;
	BOOL _compositeNamesOnly;

}
+(id)nameDataSourceForLanguageIdentifier:(id)arg1 ;
+(id)_nameDataSourceByLanguageMap;
-(unsigned long long)nameTypes;
-(int)matchingNameType:(id)arg1 fromTypes:(unsigned long long)arg2 forPerson:(void*)arg3 ;
-(unsigned long long)indexOfMainNameOfType:(int)arg1 ;
-(unsigned long long)personNameCount;
-(BOOL)getNth:(unsigned long long)arg1 name:(id*)arg2 phoneticName:(id*)arg3 ofType:(int)arg4 nameIndex:(unsigned long long*)arg5 forPerson:(void*)arg6 ;
-(BOOL)getName:(id*)arg1 phoneticName:(id*)arg2 atIndex:(unsigned long long)arg3 forPerson:(void*)arg4 ;
-(int)typeOfNameAtIndex:(unsigned long long)arg1 ;
-(unsigned long long)countOfNamesOfType:(int)arg1 ;
-(BOOL)useCompositeNamesOnly;
-(void)setUseCompositeNamesOnly:(BOOL)arg1 ;
@end

