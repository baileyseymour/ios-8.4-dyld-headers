/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:31 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/UIKit.axbundle/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIKit-Structs.h>
#import <UIKit/__UITextInputUIResponderAccessibility_super.h>

@interface UITextInputUIResponderAccessibility : __UITextInputUIResponderAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
-(CGRect)_accessibilityBoundsForRange:(NSRange)arg1 ;
-(id)_accessibilityLineNumberAndColumnForPoint:(CGPoint)arg1 ;
-(NSRange)_accessibilityRangeForLineNumberAndColumn:(id)arg1 ;
-(id)_accessibilityTextViewTextOperationResponder;
-(id)_accessibilitySpeakThisString;
-(NSRange)_accessibilityLineRangeForPosition:(unsigned long long)arg1 ;
-(void)_accessibilityCheckBorderHit:(BOOL)arg1 left:(BOOL)arg2 ;
-(long long)_accessibilityLineEndPosition;
-(long long)_accessibilityTextInputLinePosition:(BOOL)arg1 ;
-(long long)_accessibilityLineStartPosition;
-(id)_accessibilityTextRectsForSpeakThisStringRange:(NSRange)arg1 ;
-(void)_moveRight:(BOOL)arg1 withHistory:(id)arg2 ;
-(void)_moveLeft:(BOOL)arg1 withHistory:(id)arg2 ;
-(id)accessibilityValue;
-(unsigned long long)accessibilityTraits;
-(BOOL)isAccessibilityElement;
-(void)_updateSelectionWithTextRange:(id)arg1 withAffinityDownstream:(BOOL)arg2 ;
-(NSRange)_accessibilitySelectedTextRange;
-(void)_accessibilitySetSelectedTextRange:(NSRange)arg1 ;
@end

