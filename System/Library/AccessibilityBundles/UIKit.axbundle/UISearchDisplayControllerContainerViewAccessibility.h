/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:30 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/UIKit.axbundle/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/__UISearchDisplayControllerContainerViewAccessibility_super.h>

@interface UISearchDisplayControllerContainerViewAccessibility : __UISearchDisplayControllerContainerViewAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
+(void)_accessibilityPerformValidations:(id)arg1 ;
-(id)_accessibilityObscuredScreenAllowedViews;
-(BOOL)_accessibilityObscuredScreenAllowsView:(id)arg1 ;
@end

