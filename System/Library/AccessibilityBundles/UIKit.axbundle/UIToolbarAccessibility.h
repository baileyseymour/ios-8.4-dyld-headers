/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:28 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/UIKit.axbundle/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/__UIToolbarAccessibility_super.h>

@interface UIToolbarAccessibility : __UIToolbarAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
-(BOOL)_accessibilityHitTestShouldFallbackToNearestChild;
-(id)_accessibilityFuzzyHitTestElements;
-(void)_accessibilityLoadAccessibilityInformation;
-(BOOL)_accessibilityOnlyComparesByXAxis;
-(void)_showButtons:(int*)arg1 withCount:(int)arg2 group:(int)arg3 withDuration:(double)arg4 adjustPositions:(BOOL)arg5 skipTag:(int)arg6 ;
-(void)_buttonBarFinishedAnimating;
-(void)_buttonUp:(id)arg1 ;
-(unsigned long long)accessibilityTraits;
-(BOOL)shouldGroupAccessibilityChildren;
@end

