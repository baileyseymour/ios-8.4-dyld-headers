/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:32 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/UIKit.axbundle/UIKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIKit-Structs.h>
#import <UIKit/__UIDictationViewAccessibility_super.h>

@interface UIDictationViewAccessibility : __UIDictationViewAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
+(void)_accessibilityPerformValidations:(id)arg1 ;
-(id)initWithFrame:(CGRect)arg1 ;
-(void)setState:(int)arg1 ;
-(void)finishReturnToKeyboard;
-(void)endpointButtonPressed;
@end

