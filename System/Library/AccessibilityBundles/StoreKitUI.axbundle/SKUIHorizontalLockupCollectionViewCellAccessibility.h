/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:25 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/StoreKitUI.axbundle/StoreKitUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <StoreKitUI/StoreKitUI-Structs.h>
#import <StoreKitUI/__SKUIHorizontalLockupCollectionViewCellAccessibility_super.h>

@interface SKUIHorizontalLockupCollectionViewCellAccessibility : __SKUIHorizontalLockupCollectionViewCellAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
+(void)_accessibilityPerformValidations:(id)arg1 ;
-(id)_accessibilityHitTest:(CGPoint)arg1 withEvent:(id)arg2 ;
-(BOOL)accessibilityScrollToVisible;
-(void)layoutSubviews;
@end

