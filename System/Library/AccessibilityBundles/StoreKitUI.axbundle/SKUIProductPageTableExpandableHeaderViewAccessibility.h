/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:27 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/StoreKitUI.axbundle/StoreKitUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <StoreKitUI/__SKUIProductPageTableExpandableHeaderViewAccessibility_super.h>

@interface SKUIProductPageTableExpandableHeaderViewAccessibility : __SKUIProductPageTableExpandableHeaderViewAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
+(void)_accessibilityPerformValidations:(id)arg1 ;
-(id)accessibilityContainerElements;
@end

