/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:19 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/AccountsUI.axbundle/AccountsUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <AccountsUI/__ACUIAccountSummaryCellAccessibility_super.h>

@interface ACUIAccountSummaryCellAccessibility : __ACUIAccountSummaryCellAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
+(void)_accessibilityPerformValidations:(id)arg1 ;
-(id)accessibilityLabel;
@end

