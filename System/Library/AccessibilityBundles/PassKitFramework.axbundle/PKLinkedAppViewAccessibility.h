/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:22 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/PassKitFramework.axbundle/PassKitFramework
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <PassKitFramework/PassKitFramework-Structs.h>
#import <PassKitFramework/__PKLinkedAppViewAccessibility_super.h>

@interface PKLinkedAppViewAccessibility : __PKLinkedAppViewAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
-(id)accessibilityLabel;
-(id)accessibilityValue;
-(id)accessibilityHint;
-(CGPoint)accessibilityActivationPoint;
-(BOOL)isAccessibilityElement;
@end

