/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:32 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/WebProcess.axbundle/WebProcess
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <WebProcess/WebProcess-Structs.h>
#import <WebProcess/__WKNSObjectAccessibility_super.h>

@interface WKNSObjectAccessibility : __WKNSObjectAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
-(id)accessibilityAttributeValue:(int)arg1 ;
-(id)_accessibilityFirstElementForFocusForRemoteElement;
-(id)accessibilityHitTest:(CGPoint)arg1 ;
@end

