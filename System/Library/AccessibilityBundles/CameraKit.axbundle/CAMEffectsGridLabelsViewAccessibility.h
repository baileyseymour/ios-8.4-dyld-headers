/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:20 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/CameraKit.axbundle/CameraKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <CameraKit/__CAMEffectsGridLabelsViewAccessibility_super.h>

@interface CAMEffectsGridLabelsViewAccessibility : __CAMEffectsGridLabelsViewAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
-(BOOL)accessibilityElementsHidden;
@end

