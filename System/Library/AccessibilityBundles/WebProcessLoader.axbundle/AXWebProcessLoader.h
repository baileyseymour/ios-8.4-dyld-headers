/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:32 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/WebProcessLoader.axbundle/WebProcessLoader
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface AXWebProcessLoader : NSObject
+(void)_axUpdated:(id)arg1 ;
+(void)_registerForAccessibility;
+(void)initialize;
@end

