/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:23 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/PhotoLibraryFramework.axbundle/PhotoLibraryFramework
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <PhotoLibraryFramework/__PLPhotoBrowserControllerAccessibility_super.h>

@interface PLPhotoBrowserControllerAccessibility : __PLPhotoBrowserControllerAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
+(void)_accessibilityPerformValidations:(id)arg1 ;
-(void)_accessibilityLoadAccessibilityInformation;
-(void)videoRemakerDidEndRemaking:(id)arg1 temporaryPath:(id)arg2 ;
-(void)showNextImageWithTransition:(int)arg1 insideCurrentAssetContainer:(BOOL)arg2 ;
-(void)updateProgressView;
-(void)_removeProgressView;
@end

