/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:20 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/MapKitFramework.axbundle/MapKitFramework
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <MapKitFramework/__MKMapViewAccessibility_super.h>

@interface MKMapViewAccessibility : __MKMapViewAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
+(void)_accessibilityPerformValidations:(id)arg1 ;
-(id)accessibilityContainerElements;
-(void)_setCompassVisible:(BOOL)arg1 animated:(BOOL)arg2 ;
@end

