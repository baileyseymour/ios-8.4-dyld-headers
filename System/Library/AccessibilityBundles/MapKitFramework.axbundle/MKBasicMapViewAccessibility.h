/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:00:20 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/AccessibilityBundles/MapKitFramework.axbundle/MapKitFramework
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <MapKitFramework/__MKBasicMapViewAccessibility_super.h>

@interface MKBasicMapViewAccessibility : __MKBasicMapViewAccessibility_super
+(id)safeCategoryTargetClassName;
+(Class)safeCategoryTargetClass;
-(id)accessibilityContainerElements;
-(id)accessibilityCustomRotorTitles;
-(id)accessibilityCustomRotorItemsAtIndex:(long long)arg1 ;
@end

