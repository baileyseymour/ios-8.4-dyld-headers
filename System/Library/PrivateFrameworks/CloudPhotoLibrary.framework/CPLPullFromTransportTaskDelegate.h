/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:20 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/CloudPhotoLibrary.framework/CloudPhotoLibrary
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol CPLPullFromTransportTaskDelegate <CPLEngineSyncTaskDelegate>
@required
-(id)task:(id)arg1 wantsToDownloadBatchesFromSyncAnchor:(id)arg2 completionHandler:(/*^block*/id)arg3;

@end

