/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:20 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/CloudPhotoLibrary.framework/CloudPhotoLibrary
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <CloudPhotoLibrary/CPLEngineSyncTask.h>

@protocol OS_dispatch_queue;
@class NSObject, NSMutableArray, NSError;

@interface CPLBackgroundDownloadsTask : CPLEngineSyncTask {

	NSObject*<OS_dispatch_queue> _lock;
	NSMutableArray* _downloadTaskGroups;
	BOOL _shouldStop;
	NSError* _badError;
	BOOL _hasResetQueue;
	unsigned long long _successfullyDownloadedResourcesCount;
	unsigned long long _failedDownloadedResourcesCount;
	unsigned long long _total;

}
-(void)launch;
-(void)cancel;
-(id)description;
-(void)resume;
-(void)pause;
-(id)initWithEngineLibrary:(id)arg1 ;
-(void)taskDidFinishWithError:(id)arg1 ;
-(void)_finishTaskLocked;
-(void)_enqueueTasksLocked;
-(void)_downloadTask:(id)arg1 didFinishWithError:(id)arg2 ;
-(id)_downloadTasksSortedForBatching:(id)arg1 ;
-(void)_launchNecessaryDownloadTasksWithTransaction:(id)arg1 ;
-(unsigned long long)_activeDownloadTaskCount;
-(id)taskIdentifier;
@end

