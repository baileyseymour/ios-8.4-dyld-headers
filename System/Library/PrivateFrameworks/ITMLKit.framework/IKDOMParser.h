/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:20 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/ITMLKit.framework/ITMLKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <ITMLKit/IKJSObject.h>
#import <ITMLKit/IKJSDOMParser.h>

@interface IKDOMParser : IKJSObject <IKJSDOMParser>
-(id)init;
-(id)parseFromString:(id)arg1 :(id)arg2 ;
@end

