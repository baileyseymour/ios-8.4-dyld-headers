/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:54 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/StoreKitUI.framework/StoreKitUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol SKUISignInViewDelegate <NSObject>
@optional
-(void)signInEntryComplete:(id)arg1 accountName:(id)arg2 password:(id)arg3;

@end

