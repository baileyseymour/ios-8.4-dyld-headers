/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:59 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/StoreKitUI.framework/StoreKitUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSMutableDictionary;

@interface SKUIMenuPageSectionContext : NSObject {

	NSMutableDictionary* _sections;
	long long _selectedIndex;

}

@property (assign,nonatomic) long long selectedIndex;              //@synthesize selectedIndex=_selectedIndex - In the implementation block
-(long long)selectedIndex;
-(void)setSelectedIndex:(long long)arg1 ;
-(id)sectionsForIndex:(long long)arg1 ;
-(void)setSections:(id)arg1 forIndex:(long long)arg2 ;
@end

