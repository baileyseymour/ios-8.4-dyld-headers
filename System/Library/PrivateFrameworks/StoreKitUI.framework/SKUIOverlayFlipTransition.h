/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:54 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/StoreKitUI.framework/StoreKitUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class UIView;

@interface SKUIOverlayFlipTransition : NSObject {

	UIView* _flipSourceView;

}

@property (nonatomic,retain) UIView * flipSourceView;              //@synthesize flipSourceView=_flipSourceView - In the implementation block
-(void)setFlipSourceView:(UIView *)arg1 ;
-(UIView *)flipSourceView;
@end

