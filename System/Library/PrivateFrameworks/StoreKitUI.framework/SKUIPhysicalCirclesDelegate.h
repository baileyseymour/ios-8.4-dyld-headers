/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:47 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/StoreKitUI.framework/StoreKitUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol SKUIPhysicalCirclesDelegate <NSObject>
@optional
-(void)circleViewReloadDidFinish:(id)arg1;
-(void)circleView:(id)arg1 didBeginLongPressForCircleAtIndex:(long long)arg2;
-(void)circleView:(id)arg1 didEndLongPressForCircleAtIndex:(long long)arg2;
-(void)circleView:(id)arg1 didTapCircleAtIndex:(long long)arg2;

@end

