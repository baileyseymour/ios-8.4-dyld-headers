/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:23 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/RadioUI.framework/RadioUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <RadioUI/RadioUI-Structs.h>
#import <UIKit/UITableView.h>

@class UIScrollView;

@interface RUProxyTableView : UITableView {

	UIScrollView* _targetScrollView;

}

@property (assign,nonatomic,__weak) UIScrollView * targetScrollView;              //@synthesize targetScrollView=_targetScrollView - In the implementation block
-(void)setContentSize:(CGSize)arg1 ;
-(void)setTargetScrollView:(UIScrollView *)arg1 ;
-(UIScrollView *)targetScrollView;
@end

