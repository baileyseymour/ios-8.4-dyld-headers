/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:23 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/RadioUI.framework/RadioUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol RUWelcomeViewControllerDelegate <NSObject>
@optional
-(void)welcomeViewControllerDidOptIn:(id)arg1;
-(void)welcomeViewControllerDidReceiveServiceUnavailable:(id)arg1;
-(void)welcomeViewController:(id)arg1 willDisplayFailureAlert:(id)arg2;
-(void)welcomeViewController:(id)arg1 didDismissFailureAlert:(id)arg2;

@end

