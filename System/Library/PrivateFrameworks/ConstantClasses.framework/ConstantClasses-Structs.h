/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:23 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/ConstantClasses.framework/ConstantClasses
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

typedef struct {
	unsigned long long field1;
	id field2;
	unsigned long long field3;
	unsigned long long field4[5];
} SCD_Struct_NS0;

typedef struct {
	unsigned field1 : 8;
	unsigned field2 : 4;
	unsigned field3 : 1;
	unsigned field4 : 1;
	unsigned field5 : 18;
	unsigned short field6[8];
} SCD_Struct_NS1;

