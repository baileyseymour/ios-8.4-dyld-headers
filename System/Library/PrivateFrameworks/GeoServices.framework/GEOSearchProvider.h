/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:00 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/GeoServices.framework/GeoServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface GEOSearchProvider : NSObject {

	/*^block*/id _error;

}

@property (nonatomic,copy) id error;              //@synthesize error=_error - In the implementation block
-(void)cancel;
-(void)dealloc;
-(id)error;
-(void)search:(id)arg1 timeout:(long long)arg2 useBackgroundConnection:(BOOL)arg3 finished:(/*^block*/id)arg4 refinement:(/*^block*/id)arg5 error:(/*^block*/id)arg6 isCompletions:(BOOL)arg7 ;
-(void)setError:(id)arg1 ;
@end

