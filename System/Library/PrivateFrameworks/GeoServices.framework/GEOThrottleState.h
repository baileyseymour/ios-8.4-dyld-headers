/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:00 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/GeoServices.framework/GeoServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface GEOThrottleState : NSObject {

	unsigned long long _requestCount;
	double _lastResetTime;

}

@property (assign,nonatomic) unsigned long long requestCount;              //@synthesize requestCount=_requestCount - In the implementation block
@property (assign,nonatomic) double lastResetTime;                         //@synthesize lastResetTime=_lastResetTime - In the implementation block
-(unsigned long long)requestCount;
-(double)lastResetTime;
-(void)setRequestCount:(unsigned long long)arg1 ;
-(void)setLastResetTime:(double)arg1 ;
@end

