/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:00 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/GeoServices.framework/GeoServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <GeoServices/GEOPlaceSearchResponse.h>

@class NSMapTable;

@interface GEOSearchResponse : GEOPlaceSearchResponse {

	NSMapTable* _attributionKeysToInfo;

}
-(void)dealloc;
-(void)_addAttributionInfo:(id)arg1 ;
-(id)attributionInfoForSourceIdentifier:(id)arg1 sourceVersion:(unsigned)arg2 ;
@end

