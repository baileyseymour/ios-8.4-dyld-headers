/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:04 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/GeoServices.framework/GeoServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSMapTable, NSLock;

@interface GEOServiceRequester : NSObject {

	NSMapTable* _pendingRequests;
	NSLock* _pendingRequestsLock;

}
+(id)_debugRequestName;
+(unsigned long long)_urlType;
+(id)_serviceTypeNumber;
+(long long)_experimentType;
+(int)_experimentDispatcherRequestTypeForRequest:(id)arg1 ;
+(BOOL)shouldAttributeData;
+(void)setAttributeData;
-(void)dealloc;
-(id)init;
-(void)_startWithRequest:(id)arg1 traits:(id)arg2 timeout:(double)arg3 completionHandler:(/*^block*/id)arg4 ;
-(void)_cancelRequest:(id)arg1 ;
-(id)_validateResponse:(id)arg1 ;
-(void)_startWithRequest:(id)arg1 traits:(id)arg2 completionHandler:(/*^block*/id)arg3 ;
@end

