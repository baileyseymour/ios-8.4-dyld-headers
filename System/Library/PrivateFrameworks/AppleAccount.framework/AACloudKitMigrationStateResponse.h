/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:46 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/AppleAccount.framework/AppleAccount
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <AppleAccount/AAResponse.h>

@class NSString;

@interface AACloudKitMigrationStateResponse : AAResponse {

	NSString* _name;

}

@property (nonatomic,readonly) NSString * name;              //@synthesize name=_name - In the implementation block
-(NSString *)name;
-(id)initWithHTTPResponse:(id)arg1 data:(id)arg2 ;
@end

