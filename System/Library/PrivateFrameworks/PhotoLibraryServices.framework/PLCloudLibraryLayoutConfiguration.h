/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:10 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/PhotoLibraryServices.framework/PhotoLibraryServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSArray, PLPhotoLibrary;

@interface PLCloudLibraryLayoutConfiguration : NSObject {

	NSArray* layoutBuckets;
	PLPhotoLibrary* photoLibrary;

}

@property (nonatomic,retain) NSArray * layoutBuckets; 
@property (nonatomic,retain) PLPhotoLibrary * photoLibrary; 
-(PLPhotoLibrary *)photoLibrary;
-(void)setPhotoLibrary:(PLPhotoLibrary *)arg1 ;
-(NSArray *)layoutBuckets;
-(void)setLayoutBuckets:(NSArray *)arg1 ;
@end

