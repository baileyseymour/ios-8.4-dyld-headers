/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:14 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/VoiceMemos.framework/VoiceMemos
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSString;

@interface RCAudioSessionRoutingAssertion : NSObject {

	NSString* _debugLabel;
	BOOL _requiresActiveAudioSession;
	long long _priority;

}

@property (nonatomic,readonly) BOOL requiresActiveAudioSession;              //@synthesize requiresActiveAudioSession=_requiresActiveAudioSession - In the implementation block
@property (nonatomic,readonly) long long priority;                           //@synthesize priority=_priority - In the implementation block
+(id)newCaptureAssertionNamed:(id)arg1 ;
+(id)newPreviewAssertionNamed:(id)arg1 ;
+(id)newForegroundAppAssertionRequiringActiveAudioSession:(BOOL)arg1 ;
+(id)newForegroundAppAssertion;
-(void)dealloc;
-(id)init;
-(id)description;
-(long long)priority;
-(BOOL)requiresActiveAudioSession;
-(id)initWithDebugLabel:(id)arg1 requiresActiveAudioSession:(BOOL)arg2 priority:(long long)arg3 ;
@end

