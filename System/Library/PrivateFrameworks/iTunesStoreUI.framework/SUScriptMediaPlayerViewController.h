/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:31 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/iTunesStoreUI.framework/iTunesStoreUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <iTunesStoreUI/SUScriptViewController.h>

@interface SUScriptMediaPlayerViewController : SUScriptViewController
-(id)_className;
-(id)initWithMediaPlayerItem:(id)arg1 ;
@end

