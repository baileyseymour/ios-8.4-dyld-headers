/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:31 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/iTunesStoreUI.framework/iTunesStoreUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <iTunesStoreUI/SUTableCell.h>

@class SUWebDocumentView;

@interface SUMarkupCell : SUTableCell {

	SUWebDocumentView* _webView;

}
-(void)dealloc;
-(void)setBackgroundColor:(id)arg1 ;
-(id)initWithStyle:(long long)arg1 reuseIdentifier:(id)arg2 ;
-(void)setHighlighted:(BOOL)arg1 animated:(BOOL)arg2 ;
-(void)setConfiguration:(id)arg1 ;
@end

