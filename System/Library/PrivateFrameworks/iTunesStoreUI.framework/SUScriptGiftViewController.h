/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:32 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/iTunesStoreUI.framework/iTunesStoreUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <iTunesStoreUI/SUScriptViewController.h>

@class SKUIItem;

@interface SUScriptGiftViewController : SUScriptViewController {

	long long _giftCategory;
	SKUIItem* _item;

}

@property (nonatomic,readonly) long long giftCategoryApps; 
@property (nonatomic,readonly) long long giftCategoryBooks; 
@property (nonatomic,readonly) long long giftCategoryMedia; 
+(void)initialize;
+(id)webScriptNameForSelector:(SEL)arg1 ;
+(id)webScriptNameForKeyName:(id)arg1 ;
-(id)scriptAttributeKeys;
-(id)_className;
-(id)attributeKeys;
-(id)newNativeViewController;
-(void)setCreditGiftStyle:(long long)arg1 ;
-(void)setProductGiftItem:(id)arg1 ;
-(long long)giftCategoryApps;
-(long long)giftCategoryBooks;
-(long long)giftCategoryMedia;
@end

