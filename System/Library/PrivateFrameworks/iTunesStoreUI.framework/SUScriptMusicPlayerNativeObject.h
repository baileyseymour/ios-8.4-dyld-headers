/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:30 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/iTunesStoreUI.framework/iTunesStoreUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <iTunesStoreUI/SUScriptNativeObject.h>

@interface SUScriptMusicPlayerNativeObject : SUScriptNativeObject
-(void)destroyNativeObject;
-(void)setupNativeObject;
-(void)_nowPlayingChangeNotification:(id)arg1 ;
-(void)_playbackStateChangeNotification:(id)arg1 ;
-(void)_volumeChangeNotification:(id)arg1 ;
@end

