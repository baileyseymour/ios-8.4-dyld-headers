/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:31 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/iTunesStoreUI.framework/iTunesStoreUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSMutableArray;

@interface SUDelayedQuitController : NSObject {

	long long _delayTerminateCount;
	NSMutableArray* _longLivedViewControllers;

}
+(id)sharedInstance;
+(void)beginDelayingTerminate;
+(void)endDelayingTerminate;
+(BOOL)isDelayingTerminate;
+(void)checkInLongLivedViewController:(id)arg1 ;
+(void)checkOutLongLivedViewController:(id)arg1 ;
+(BOOL)viewControllerIsLongLived:(id)arg1 ;
+(id)checkedInViewControllerOfClass:(Class)arg1 ;
-(void)dealloc;
-(void)_checkInLongLivedViewController:(id)arg1 ;
-(void)_checkOutLongLivedViewController:(id)arg1 ;
-(BOOL)_viewControllerIsLongLived:(id)arg1 ;
-(id)_checkedInViewControllerOfClass:(Class)arg1 ;
-(BOOL)_isDelayingTerminate;
-(void)_beginDelayingTerminate;
-(void)_endDelayingTerminate;
@end

