/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:30 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/iTunesStoreUI.framework/iTunesStoreUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <iTunesStoreUI/SUScriptViewController.h>

@interface SUScriptDownloadsViewController : SUScriptViewController

@property (retain) id buttons; 
+(void)initialize;
+(id)webScriptNameForKeyName:(id)arg1 ;
-(id)buttons;
-(id)scriptAttributeKeys;
-(id)_className;
-(void)setButtons:(id)arg1 ;
-(id)attributeKeys;
-(id)_nativeViewController;
-(id)newNativeViewController;
@end

