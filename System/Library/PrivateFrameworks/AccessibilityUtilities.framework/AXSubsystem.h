/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:39 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/AccessibilityUtilities.framework/AccessibilityUtilities
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface AXSubsystem : NSObject
+(BOOL)shouldProcessMessageForLogs;
+(BOOL)shouldIncludeBacktraceInLogs;
+(BOOL)shouldLogToFile;
+(id)identifier;
+(void)initialize;
+(int)defaultLogLevel;
+(id)_errorWithMessage:(id)arg1 underlyingError:(id)arg2 ;
+(int)effectiveLogLevel;
+(id)subsystems;
+(id)errorWithDescription:(id)arg1 ;
+(id)wrapError:(id)arg1 description:(id)arg2 ;
+(void)setPreferredLogLevel:(int)arg1 ;
+(void)resetPreferredLogLevel;
+(BOOL)willLog;
+(void)setShouldLogToFile:(BOOL)arg1 ;
@end

