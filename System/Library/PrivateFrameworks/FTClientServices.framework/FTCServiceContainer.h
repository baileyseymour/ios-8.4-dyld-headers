/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:44 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/FTClientServices.framework/FTClientServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class FTCServiceMonitor, NSMutableSet;

@interface FTCServiceContainer : NSObject {

	FTCServiceMonitor* _monitor;
	NSMutableSet* _listeners;

}

@property (nonatomic,retain) FTCServiceMonitor * monitor;                    //@synthesize monitor=_monitor - In the implementation block
@property (nonatomic,retain,readonly) NSMutableSet * listeners;              //@synthesize listeners=_listeners - In the implementation block
-(id)initWithServiceType:(long long)arg1 ;
-(void)dealloc;
-(NSMutableSet *)listeners;
-(BOOL)hasListenerID:(id)arg1 ;
-(BOOL)addListenerID:(id)arg1 ;
-(BOOL)removeListenerID:(id)arg1 ;
-(FTCServiceMonitor *)monitor;
-(void)setMonitor:(FTCServiceMonitor *)arg1 ;
@end

