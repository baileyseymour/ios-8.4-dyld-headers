/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:09 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/ChatKit.framework/ChatKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <ChatKit/CKStampChatItem.h>

@class NSDate;

@interface CKDateChatItem : CKStampChatItem

@property (nonatomic,retain,readonly) NSDate * date; 
+(id)thePastDateFormatter;
+(id)thisWeekRelativeDateFormatter;
+(id)thisYearDateFormatter;
-(id)now;
-(NSDate *)date;
-(id)loadTranscriptText;
@end

