/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:06 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/ChatKit.framework/ChatKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol CKMessageEntryContentViewDelegate <UIScrollViewDelegate>
@required
-(void)messageEntryContentViewDidChange:(id)arg1;
-(BOOL)messageEntryContentViewShouldBeginEditing:(id)arg1;
-(void)messageEntryContentViewDidBeginEditing:(id)arg1;
-(void)messageEntryContentViewDidEndEditing:(id)arg1;
-(BOOL)messageEntryContentView:(id)arg1 shouldInsertMediaObjects:(id)arg2;

@end

