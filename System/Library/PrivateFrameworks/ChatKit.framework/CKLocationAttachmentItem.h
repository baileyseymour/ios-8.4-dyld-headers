/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:07 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/ChatKit.framework/ChatKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <ChatKit/ChatKit-Structs.h>
#import <ChatKit/CKAttachmentItem.h>

@interface CKLocationAttachmentItem : CKAttachmentItem {

	CGSize _coordinate;

}

@property (assign,nonatomic) CGSize coordinate;              //@synthesize coordinate=_coordinate - In the implementation block
+(id)UTITypes;
-(void)generatePreviewWithCompletion:(/*^block*/id)arg1 ;
-(id)initWithFileURL:(id)arg1 size:(CGSize)arg2 guid:(id)arg3 ;
-(id)_generateThumbnailFillToSize:(CGSize)arg1 ;
-(id)pin;
-(id)vCardURLProperties;
-(BOOL)isDroppedPin;
-(CGSize)coordinate;
-(void)setCoordinate:(CGSize)arg1 ;
@end

