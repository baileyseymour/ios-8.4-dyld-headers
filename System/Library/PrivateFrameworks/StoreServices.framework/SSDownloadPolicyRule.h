/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:02 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/StoreServices.framework/StoreServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <StoreServices/StoreServices-Structs.h>
#import <StoreServices/NSCoding.h>
#import <StoreServices/SSXPCCoding.h>
#import <StoreServices/NSCopying.h>

@class NSSet, NSString;

@interface SSDownloadPolicyRule : NSObject <NSCoding, SSXPCCoding, NSCopying> {

	NSSet* _applicationStates;
	float _batteryLevel;
	long long _cellularDataStates;
	long long _downloadSizeLimit;
	NSSet* _networkTypes;
	long long _powerStates;
	long long _registrationStates;
	long long _timeLimitStates;

}

@property (assign,nonatomic) long long downloadSizeLimit;                                  //@synthesize downloadSizeLimit=_downloadSizeLimit - In the implementation block
@property (nonatomic,copy) NSSet * applicationStates;                                      //@synthesize applicationStates=_applicationStates - In the implementation block
@property (nonatomic,copy) NSSet * networkTypes;                                           //@synthesize networkTypes=_networkTypes - In the implementation block
@property (getter=isCellularAllowed,nonatomic,readonly) BOOL cellularAllowed; 
@property (getter=isWiFiAllowed,nonatomic,readonly) BOOL wiFiAllowed; 
@property (assign,nonatomic) float batteryLevel;                                           //@synthesize batteryLevel=_batteryLevel - In the implementation block
@property (assign,nonatomic) long long cellularDataStates;                                 //@synthesize cellularDataStates=_cellularDataStates - In the implementation block
@property (assign,nonatomic) long long powerStates;                                        //@synthesize powerStates=_powerStates - In the implementation block
@property (assign,nonatomic) long long registrationStates;                                 //@synthesize registrationStates=_registrationStates - In the implementation block
@property (assign,nonatomic) long long timeLimitStates;                                    //@synthesize timeLimitStates=_timeLimitStates - In the implementation block
@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
-(id)copyXPCEncoding;
-(id)initWithXPCEncoding:(id)arg1 ;
-(long long)downloadSizeLimit;
-(void)setCellularDataStates:(long long)arg1 ;
-(void)unionPolicyRule:(id)arg1 ;
-(void)dealloc;
-(id)initWithCoder:(id)arg1 ;
-(void)encodeWithCoder:(id)arg1 ;
-(id)init;
-(BOOL)isEqual:(id)arg1 ;
-(unsigned long long)hash;
-(id)copyWithZone:(NSZone*)arg1 ;
-(float)batteryLevel;
-(NSSet *)networkTypes;
-(NSSet *)applicationStates;
-(void)addApplicationState:(id)arg1 ;
-(void)setApplicationStates:(NSSet *)arg1 ;
-(long long)cellularDataStates;
-(long long)powerStates;
-(long long)registrationStates;
-(long long)timeLimitStates;
-(void)addNetworkType:(long long)arg1 ;
-(BOOL)isCellularAllowed;
-(BOOL)isWiFiAllowed;
-(void)setBatteryLevel:(float)arg1 ;
-(void)setDownloadSizeLimit:(long long)arg1 ;
-(void)setNetworkTypes:(NSSet *)arg1 ;
-(void)setPowerStates:(long long)arg1 ;
-(void)setRegistrationStates:(long long)arg1 ;
-(void)setTimeLimitStates:(long long)arg1 ;
@end

