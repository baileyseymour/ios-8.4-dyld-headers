/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:03 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/StoreServices.framework/StoreServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSData, NSURLRequest, SSURLRequestProperties, NSHTTPURLResponse, NSString;

@interface SSVSAPSignatureDataSource : NSObject {

	NSData* _bodyData;
	NSURLRequest* _request;
	SSURLRequestProperties* _requestProperties;
	NSHTTPURLResponse* _response;

}

@property (nonatomic,readonly) NSData * HTTPBody; 
@property (nonatomic,readonly) NSString * HTTPMethod; 
-(NSData *)HTTPBody;
-(id)initWithURLRequest:(id)arg1 ;
-(NSString *)HTTPMethod;
-(id)valueForQueryParameter:(id)arg1 ;
-(id)initWithURLRequestProperties:(id)arg1 ;
-(id)initWithURLResponse:(id)arg1 bodyData:(id)arg2 ;
-(id)valueForHTTPHeader:(id)arg1 ;
@end

