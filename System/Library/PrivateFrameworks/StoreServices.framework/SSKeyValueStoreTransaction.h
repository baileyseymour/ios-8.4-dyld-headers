/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:02 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/StoreServices.framework/StoreServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <StoreServices/SSKeyValueStoreSession.h>

@interface SSKeyValueStoreTransaction : SSKeyValueStoreSession
-(BOOL)setValue:(id)arg1 forDomain:(id)arg2 key:(id)arg3 ;
-(void)removeAccountFromDomain:(id)arg1 ;
-(BOOL)setData:(id)arg1 forDomain:(id)arg2 key:(id)arg3 ;
@end

