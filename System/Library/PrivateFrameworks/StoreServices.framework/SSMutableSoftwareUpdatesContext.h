/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:01 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/StoreServices.framework/StoreServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <StoreServices/SSSoftwareUpdatesContext.h>
#import <StoreServices/SSXPCCoding.h>

@class NSArray, NSString;

@interface SSMutableSoftwareUpdatesContext : SSSoftwareUpdatesContext <SSXPCCoding>

@property (assign,getter=isForced,nonatomic) BOOL forced; 
@property (nonatomic,copy) NSArray * softwareTypes; 
@property (nonatomic,copy) NSString * clientIdentifierHeader; 
@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
-(id)copyXPCEncoding;
-(id)initWithXPCEncoding:(id)arg1 ;
-(void)setClientIdentifierHeader:(NSString *)arg1 ;
-(void)setForced:(BOOL)arg1 ;
-(void)setSoftwareTypes:(NSArray *)arg1 ;
@end

