/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:45 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/NearField.framework/NearField
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSString, NSDecimalNumber, NSData;

@interface NFTransactionEndEvent : NSObject {

	unsigned short _command;
	unsigned short _status;
	unsigned short _type;
	unsigned short _result;
	unsigned short _informative;
	NSString* _aid;
	NSString* _transactionId;
	NSDecimalNumber* _amount;
	NSString* _currency;
	NSData* _tlv;

}

@property (nonatomic,readonly) NSString * aid;                          //@synthesize aid=_aid - In the implementation block
@property (nonatomic,readonly) NSString * transactionId;                //@synthesize transactionId=_transactionId - In the implementation block
@property (nonatomic,readonly) unsigned short command;                  //@synthesize command=_command - In the implementation block
@property (nonatomic,readonly) unsigned short status;                   //@synthesize status=_status - In the implementation block
@property (nonatomic,readonly) unsigned short type;                     //@synthesize type=_type - In the implementation block
@property (nonatomic,readonly) unsigned short result;                   //@synthesize result=_result - In the implementation block
@property (nonatomic,readonly) unsigned short informative;              //@synthesize informative=_informative - In the implementation block
@property (nonatomic,readonly) NSDecimalNumber * amount;                //@synthesize amount=_amount - In the implementation block
@property (nonatomic,readonly) NSString * currency;                     //@synthesize currency=_currency - In the implementation block
@property (nonatomic,readonly) NSData * tlv;                            //@synthesize tlv=_tlv - In the implementation block
-(unsigned short)command;
-(void)dealloc;
-(id)description;
-(id)initWithDictionary:(id)arg1 ;
-(unsigned short)type;
-(NSDecimalNumber *)amount;
-(unsigned short)result;
-(unsigned short)status;
-(NSData *)tlv;
-(NSString *)currency;
-(NSString *)aid;
-(NSString *)transactionId;
-(unsigned short)informative;
@end

