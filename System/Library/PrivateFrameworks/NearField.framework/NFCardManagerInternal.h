/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:45 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/NearField.framework/NearField
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <NearField/XPCConnectionDelegate.h>

@class XPCConnection, NSDictionary, NSString;

@interface NFCardManagerInternal : NSObject <XPCConnectionDelegate> {

	XPCConnection* _xpcConnection;
	NSDictionary* _infoCache;

}

@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
-(void)dealloc;
-(id)_getXPCConnection;
-(void)XPCConnectionDidTerminate:(id)arg1 ;
-(void)XPCConnectionServiceDidRestart:(id)arg1 ;
-(unsigned)getSEInfoCache:(id*)arg1 ;
-(id)getCards;
-(unsigned)signChallenge:(id)arg1 response:(id*)arg2 ;
-(unsigned)authorizeForContactless:(id)arg1 ;
-(unsigned)authorizeForECommerce:(id)arg1 parameters:(id)arg2 response:(id*)arg3 ;
-(unsigned)deauthorize;
-(unsigned)cardWithAID:(id)arg1 card:(id*)arg2 ;
-(unsigned)installedAIDs:(id*)arg1 ;
-(unsigned)setCardsWithAIDs:(id)arg1 asActive:(BOOL)arg2 ;
-(unsigned)deleteAllCards;
-(unsigned)activateOnlyCardWithAID:(id)arg1 ;
-(unsigned)setRegistrationState:(BOOL)arg1 ;
-(unsigned)setRegionSpecificRegistrationInfo:(id)arg1 ;
-(unsigned)connectToServerForTopic:(id)arg1 ;
-(unsigned)connectToServer:(id)arg1 ;
-(unsigned)queueServerConnection;
-(unsigned)queueServerConnectionForPushTopics:(id)arg1 ;
-(unsigned)queueServerConnectionForAids:(id)arg1 ;
-(unsigned)serverRegistrationInfo:(id*)arg1 ;
-(unsigned)nextPushInfo:(id*)arg1 ;
-(unsigned)isInRestrictedMode;
-(unsigned)isSecureElementAvailable;
-(unsigned)logRegistrationState;
-(unsigned)serialNumber:(id*)arg1 ;
-(unsigned)certificates:(id*)arg1 ;
-(unsigned)isProductionSigned:(BOOL*)arg1 ;
-(unsigned)deleteCardsWithAIDs:(id)arg1 ;
@end

