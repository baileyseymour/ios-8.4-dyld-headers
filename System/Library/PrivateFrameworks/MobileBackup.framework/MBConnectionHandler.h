/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:36 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/MobileBackup.framework/MobileBackup
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol MBConnectionHandler
@required
-(void)connectionWasInterrupted:(id)arg1;
-(void)connection:(id)arg1 didReceiveMessage:(id)arg2;
-(void)connectionWasInvalid:(id)arg1;

@end

