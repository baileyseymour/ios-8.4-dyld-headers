/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:22 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/ImageCapture.framework/ImageCapture
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface PTPCancelPacket : NSObject {

	unsigned _transactionID;

}
-(id)description;
-(unsigned)transactionID;
-(id)contentForTCP;
-(id)initWithTCPBuffer:(void*)arg1 ;
-(id)initWithTransactionID:(unsigned)arg1 ;
-(void)setTransactionID:(unsigned)arg1 ;
@end

