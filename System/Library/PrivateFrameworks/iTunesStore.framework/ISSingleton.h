/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:25 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/iTunesStore.framework/iTunesStore
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol ISSingleton <NSObject>
@required
+(id)sharedInstance;
+(void)setSharedInstance:(id)arg1;

@end

