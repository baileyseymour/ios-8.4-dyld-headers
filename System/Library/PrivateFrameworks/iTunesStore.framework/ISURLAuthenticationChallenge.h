/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:26 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/iTunesStore.framework/iTunesStore
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <iTunesStore/ISAuthenticationChallenge.h>

@class NSURLAuthenticationChallenge;

@interface ISURLAuthenticationChallenge : ISAuthenticationChallenge {

	NSURLAuthenticationChallenge* _challenge;

}
-(BOOL)hasPassword;
-(void)dealloc;
-(id)sender;
-(id)password;
-(id)user;
-(long long)failureCount;
-(void)cancelAuthentication;
-(id)initWithAuthenticationChallenge:(id)arg1 ;
-(void)useCredential:(id)arg1 ;
@end

