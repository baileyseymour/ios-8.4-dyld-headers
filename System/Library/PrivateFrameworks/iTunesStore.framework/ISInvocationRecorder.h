/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:25 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/iTunesStore.framework/iTunesStore
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface ISInvocationRecorder : NSObject {

	id _target;

}
-(void)dealloc;
-(id)methodSignatureForSelector:(SEL)arg1 ;
-(id)initWithTarget:(id)arg1 ;
-(void)forwardInvocation:(id)arg1 ;
-(void)invokeInvocation:(id)arg1 ;
-(id)adjustedTargetForSelector:(SEL)arg1 ;
@end

