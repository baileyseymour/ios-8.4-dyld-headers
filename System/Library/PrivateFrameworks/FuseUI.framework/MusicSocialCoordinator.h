/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:53 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/FuseUI.framework/FuseUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol OS_dispatch_queue;
@class NSObject, NSMutableArray, NSOperationQueue;

@interface MusicSocialCoordinator : NSObject {

	NSObject*<OS_dispatch_queue> _accessQueue;
	NSMutableArray* _following;
	BOOL _followingLoaded;
	NSOperationQueue* _queue;

}
+(id)sharedInstance;
+(id)activeDsid;
-(id)init;
-(id)_following;
-(void)requestFollowingStateForArtistWithStoreID:(long long)arg1 completion:(/*^block*/id)arg2 ;
-(void)updateFollowingState:(BOOL)arg1 forArtistWithStoreID:(long long)arg2 completion:(/*^block*/id)arg3 ;
-(void)_processFollowing:(id)arg1 ;
-(void)loadFollowingWithCompletionHandler:(/*^block*/id)arg1 ;
@end

