/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:54 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/FuseUI.framework/FuseUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <FuseUI/MusicLibraryBrowseCollectionViewConfiguration.h>

@class NSArray, MusicEntityCollectionViewDescriptor;

@interface MusicLibraryBrowseHeterogenousCollectionViewConfiguration : MusicLibraryBrowseCollectionViewConfiguration {

	NSArray* _libraryViewConfigurations;

}

@property (nonatomic,readonly) NSArray * libraryViewConfigurations;                                               //@synthesize libraryViewConfigurations=_libraryViewConfigurations - In the implementation block
@property (nonatomic,readonly) MusicEntityCollectionViewDescriptor * entityCollectionViewDescriptor; 
-(id)libraryViewConfigurationForSection:(unsigned long long)arg1 ;
-(id)collectionViewDescriptorForSection:(unsigned long long)arg1 traitCollection:(id)arg2 ;
-(NSArray *)libraryViewConfigurations;
-(id)initWithLibraryViewConfigurations:(id)arg1 ;
@end

