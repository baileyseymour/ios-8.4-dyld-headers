/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:49 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/FuseUI.framework/FuseUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol MusicMediaDetailSplitViewController <NSObject>
@property (assign,nonatomic,__weak) id<MusicMediaDetailSplitViewControllerDelegate> mediaSplitViewControllerDelegate; 
@optional
-(void)setMediaDetailTintInformation:(id)arg1;

@required
-(id<MusicMediaDetailSplitViewControllerDelegate>)mediaSplitViewControllerDelegate;
-(void)setMediaSplitViewControllerDelegate:(id)arg1;

@end

