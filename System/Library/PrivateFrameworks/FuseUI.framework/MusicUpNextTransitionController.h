/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:53 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/FuseUI.framework/FuseUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIViewControllerAnimatedTransitioning.h>

@class MusicNowPlayingViewController, MusicUpNextViewController, UIView, NSString;

@interface MusicUpNextTransitionController : NSObject <UIViewControllerAnimatedTransitioning> {

	/*^block*/id _transitionStartBlock;
	/*^block*/id _transitionFinishBlock;
	/*^block*/id _transitionEndBlock;
	MusicNowPlayingViewController* _nowPlayingViewController;
	MusicUpNextViewController* _upNextViewController;
	BOOL _requiresNonSpringAnimation;
	BOOL _presenting;
	UIView* _itemContainerView;

}

@property (assign,getter=isPresenting,nonatomic) BOOL presenting;              //@synthesize presenting=_presenting - In the implementation block
@property (assign,nonatomic,__weak) UIView * itemContainerView;                //@synthesize itemContainerView=_itemContainerView - In the implementation block
@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
-(double)transitionDuration:(id)arg1 ;
-(void)animateTransition:(id)arg1 ;
-(void)setPresenting:(BOOL)arg1 ;
-(BOOL)isPresenting;
-(void)setItemContainerView:(UIView *)arg1 ;
-(void)_prepareTransitionForUnderArtwork:(id)arg1 ;
-(UIView *)itemContainerView;
@end

