/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:50 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/FuseUI.framework/FuseUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <Foundation/NSOperation.h>

@class MPUContentItemIdentifierCollection;

@interface MusicLibraryActionAddOperation : NSOperation {

	MPUContentItemIdentifierCollection* _contentItemIdentifierCollection;

}

@property (copy,readonly) MPUContentItemIdentifierCollection * contentItemIdentifierCollection;              //@synthesize contentItemIdentifierCollection=_contentItemIdentifierCollection - In the implementation block
+(BOOL)canLibraryAddWithContentItemIdentifierCollection:(id)arg1 ;
-(void)main;
-(id)initWithContentItemIdentifierCollection:(id)arg1 ;
-(MPUContentItemIdentifierCollection *)contentItemIdentifierCollection;
@end

