/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:54 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/FuseUI.framework/FuseUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol SKUIModalDocumentDataSource <NSObject>
@required
-(id)modalDocumentController:(id)arg1 alertControllerForDocument:(id)arg2 withDismissObserverBlock:(/*^block*/id)arg3 options:(id)arg4;

@end

