/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:07 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/TelephonyUtilities.framework/TelephonyUtilities
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSArray;

@interface TUCallGroup : NSObject {

	NSArray* _calls;

}

@property (retain) NSArray * calls;              //@synthesize calls=_calls - In the implementation block
-(void)dealloc;
-(BOOL)isEqual:(id)arg1 ;
-(id)description;
-(id)methodSignatureForSelector:(SEL)arg1 ;
-(void)forwardInvocation:(id)arg1 ;
-(id)displayName;
-(int)status;
-(NSArray *)calls;
-(void)setCalls:(NSArray *)arg1 ;
@end

