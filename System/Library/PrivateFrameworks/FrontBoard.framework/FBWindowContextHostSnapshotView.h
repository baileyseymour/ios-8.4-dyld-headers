/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:47 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/FrontBoard.framework/FrontBoard
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <FrontBoard/FrontBoard-Structs.h>
#import <UIKit/UIView.h>
#import <FrontBoard/FBWindowContextHostSnapshotView.h>

@protocol FBWindowContextHostSnapshotView
@property (nonatomic,readonly) id IOSurface; 
@required
-(id)IOSurface;

@end


@interface FBWindowContextHostSnapshotView : UIView <FBWindowContextHostSnapshotView> {

	id _surface;

}

@property (nonatomic,readonly) id IOSurface;              //@synthesize surface=_surface - In the implementation block
-(id)initWithFrame:(CGRect)arg1 ;
-(id)_initWithFrame:(CGRect)arg1 CGImage:(CGImageRef)arg2 transform:(CGAffineTransform)arg3 surface:(id)arg4 ;
-(id)IOSurface;
@end

