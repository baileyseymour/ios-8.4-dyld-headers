/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:07 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/HealthDaemon.framework/HealthDaemon
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <HealthDaemon/HealthDaemon-Structs.h>
@interface _HDDiscreteStatsWrapper : NSObject {

	SCD_Struct_HD0 stats;

}
-(id)description;
-(id)initWithStats:(const SCD_Struct_HD0*)arg1 ;
@end

