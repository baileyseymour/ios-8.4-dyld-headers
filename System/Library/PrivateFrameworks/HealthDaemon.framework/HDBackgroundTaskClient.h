/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:08 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/HealthDaemon.framework/HealthDaemon
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface HDBackgroundTaskClient : NSObject {

	/*^block*/id _taskHandler;

}
-(id)initWithTaskHandler:(/*^block*/id)arg1 ;
-(void)deliverTask:(id)arg1 taskName:(id)arg2 onQueue:(id)arg3 ;
@end

