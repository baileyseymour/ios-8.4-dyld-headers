/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:07 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/HealthDaemon.framework/HealthDaemon
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <HealthDaemon/HDSampleSyncEntity.h>

@interface HDCorrelationSampleSyncEntity : HDSampleSyncEntity
+(long long)syncEntityType;
+(Class)healthEntityClass;
+(id)_objectWithCodable:(id)arg1 ;
@end

