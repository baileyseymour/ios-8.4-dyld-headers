/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:24 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/iAdDeveloper.framework/iAdDeveloper
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <iAdDeveloper/iAdDeveloper-Structs.h>
#import <iAdDeveloper/HTSHTTPMessage.h>

@class NSString, NSURL;

@interface HTSHTTPMessageRequest : HTSHTTPMessage {

	NSString* _requestMethod;
	NSURL* _requestURL;

}

@property (nonatomic,copy) NSString * requestMethod;              //@synthesize requestMethod=_requestMethod - In the implementation block
@property (nonatomic,copy) NSURL * requestURL;                    //@synthesize requestURL=_requestURL - In the implementation block
-(void)setRequestMethod:(NSString *)arg1 ;
-(void)dealloc;
-(id)initWithRequest:(CFHTTPMessageRef)arg1 ;
-(NSURL *)requestURL;
-(id)responseWithStatus:(int)arg1 ;
-(void)setRequestURL:(NSURL *)arg1 ;
-(CFHTTPMessageRef)copyMessage;
-(BOOL)responseCanUseGZip;
-(NSString *)requestMethod;
@end

