/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:26 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/ManagedConfiguration.framework/ManagedConfiguration
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface MCLazyInitializationUtilities : NSObject
+(void)loadBundleAtURL:(id)arg1 completionBlock:(/*^block*/id)arg2 ;
+(void)loadNSBundleAtURL:(id)arg1 completionBlock:(/*^block*/id)arg2 ;
+(void)initVPNUtilities;
+(void)initPowerlog;
+(void)initCoreGraphics;
+(void)initAppleKeyStore;
+(void)initAddressBook;
+(void)initImageIO;
+(void)initCoreText;
+(void)initDataAccess;
+(void)initDAEAS;
@end

