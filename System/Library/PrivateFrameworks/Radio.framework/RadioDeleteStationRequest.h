/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:20 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/Radio.framework/Radio
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <Radio/RadioSyncRequest.h>

@class NSDictionary;

@interface RadioDeleteStationRequest : RadioSyncRequest {

	NSDictionary* _stationDictionary;
	unsigned long long _stationID;

}
-(id)initWithStation:(id)arg1 ;
-(id)init;
-(void)startWithCompletionHandler:(/*^block*/id)arg1 ;
-(id)changeList;
@end

