/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:27 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/CoreMediaStream.framework/CoreMediaStream
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol MSPublishStreamsProtocolDelegate <MSStreamsProtocolDelegate>
@required
-(void)publishStreamsProtocol:(id)arg1 didFinishUploadingMetadataResponse:(id)arg2 error:(id)arg3;
-(void)publishStreamsProtocol:(id)arg1 didReceiveAuthenticationError:(id)arg2;
-(void)publishStreamsProtocol:(id)arg1 didFinishSendingUploadCompleteError:(id)arg2;

@end

