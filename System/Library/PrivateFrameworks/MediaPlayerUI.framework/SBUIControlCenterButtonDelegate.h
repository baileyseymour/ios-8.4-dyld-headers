/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:29 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/MediaPlayerUI.framework/MediaPlayerUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol SBUIControlCenterButtonDelegate <NSObject>
@optional
-(void)button:(id)arg1 didChangeState:(long long)arg2;

@required
-(void)buttonTapped:(id)arg1;

@end

