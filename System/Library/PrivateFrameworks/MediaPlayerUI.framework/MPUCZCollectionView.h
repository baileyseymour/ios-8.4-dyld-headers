/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:28 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/MediaPlayerUI.framework/MediaPlayerUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <MediaPlayerUI/MediaPlayerUI-Structs.h>
#import <UIKit/UICollectionView.h>

@interface MPUCZCollectionView : UICollectionView
-(id)initWithFrame:(CGRect)arg1 collectionViewLayout:(id)arg2 ;
@end

