/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:51 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/BackBoardServices.framework/BackBoardServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol OS_dispatch_queue;
@class NSObject, BKSApplicationDataStoreRepositoryClient;

@interface BKSApplicationDataStoreClientFactory : NSObject {

	unsigned long long _count;
	NSObject*<OS_dispatch_queue> _queue;
	BKSApplicationDataStoreRepositoryClient* _sharedClient;

}
+(id)sharedInstance;
-(void)dealloc;
-(id)init;
-(void)checkin;
-(id)checkout;
@end

