/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:31 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/MediaSocial.framework/MediaSocial
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIAlertController.h>

@interface MSCLAccountErrorAlertController : UIAlertController
+(id)alertControllerWithError:(id)arg1 clientContext:(id)arg2 ;
@end

