/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:37 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/AXHearingAidSupport.framework/AXHearingAidSupport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <AXHearingAidSupport/AXHearingAidSupport-Structs.h>
@class NSMutableData;

@interface AXHAPacket : NSObject {

	unsigned long long _totalLength;
	NSMutableData* _data;

}

@property (nonatomic,retain) NSMutableData * data;              //@synthesize data=_data - In the implementation block
+(id)packetDataWithPayload:(id)arg1 ;
+(SCD_Struct_AX2)headerFromData:(id)arg1 ;
+(id)packetWithHeader:(SCD_Struct_AX2)arg1 ;
-(void)dealloc;
-(NSMutableData *)data;
-(void)setData:(NSMutableData *)arg1 ;
-(id)appendData:(id)arg1 ;
-(id)objectPayload;
-(unsigned long long)missingLength;
-(id)initWithHeader:(SCD_Struct_AX2)arg1 ;
-(id)dataPayload;
@end

