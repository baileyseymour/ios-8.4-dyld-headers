/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:14 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/PowerlogLiteOperators.framework/PowerlogLiteOperators
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <PowerlogCore/PLAgent.h>

@class PLXPCListenerOperatorComposition;

@interface PLHealthKitAgent : PLAgent {

	PLXPCListenerOperatorComposition* _queryHandler;

}

@property (retain) PLXPCListenerOperatorComposition * queryHandler;              //@synthesize queryHandler=_queryHandler - In the implementation block
+(void)load;
+(id)entryEventPointDefinitions;
-(void)initOperatorDependancies;
-(PLXPCListenerOperatorComposition *)queryHandler;
-(void)setQueryHandler:(PLXPCListenerOperatorComposition *)arg1 ;
@end

