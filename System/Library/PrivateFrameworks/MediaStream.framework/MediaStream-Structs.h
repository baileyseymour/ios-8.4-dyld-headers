/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:33 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/MediaStream.framework/MediaStream
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

typedef struct __CFUserNotification* CFUserNotificationRef;

typedef struct __CFRunLoopSource* CFRunLoopSourceRef;

typedef struct __CFString* CFStringRef;

typedef struct IONotificationPort* IONotificationPortRef;

