/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:16 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/WebContentAnalysis.framework/WebContentAnalysis
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <WebContentAnalysis/WebContentAnalysis-Structs.h>
#import <WebContentAnalysis/WFLSMMap.h>

@interface WFLSMScoreNormalizedMap : WFLSMMap {

	float* maxScore;
	float* minScore;

}
-(void)dealloc;
-(id)initWithMap:(LSMMapRef)arg1 ;
-(id)evaluate:(id)arg1 ;
@end

