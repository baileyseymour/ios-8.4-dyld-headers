/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:10 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/TouchRemote.framework/TouchRemote
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol OS_dispatch_queue;
@class NSObject, NSMutableArray;

@interface TRBrowser : NSObject {

	NSObject*<OS_dispatch_queue> _accessQueue;
	NSMutableArray* _services;
	unsigned long long _browsingCount;

}
+(id)sharedBrowser;
-(void)dealloc;
-(id)init;
-(id)_init;
-(void)stopBrowsing;
-(void)startBrowsing;
-(void)_XPCClientConnectionDidInterruptNotification:(id)arg1 ;
-(void)getServicesWithCompletionHandler:(/*^block*/id)arg1 ;
-(void)TRXPCC_browserDidFindService:(id)arg1 ;
-(void)TRXPCC_browserDidRemoveService:(id)arg1 ;
@end

