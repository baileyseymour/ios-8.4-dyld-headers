/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:42 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/SpringBoardFoundation.framework/SpringBoardFoundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSString, SBFWeakReference;

@interface SBFPasscodeLockDisableAssertion : NSObject {

	NSString* _identifier;
	int _type;
	SBFWeakReference* _weakController;

}
-(void)dealloc;
-(id)description;
-(int)type;
-(id)initWithIdentifier:(id)arg1 type:(int)arg2 withController:(id)arg3 ;
@end

