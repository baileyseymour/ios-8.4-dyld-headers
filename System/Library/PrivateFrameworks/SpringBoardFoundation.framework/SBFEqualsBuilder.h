/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:42 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/SpringBoardFoundation.framework/SpringBoardFoundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <SpringBoardFoundation/SpringBoardFoundation-Structs.h>
@interface SBFEqualsBuilder : NSObject
+(BOOL)evaluateBuilderBlock:(/*^block*/id)arg1 remainingBlocks:(char*)arg2 ;
+(BOOL)isObject:(id)arg1 equalToOther:(id)arg2 withBlocks:(/*^block*/id)arg3 ;
+(BOOL)isObject:(id)arg1 kindOfClass:(Class)arg2 andEqualToObject:(id)arg3 withBlocks:(/*^block*/id)arg4 ;
+(BOOL)isObject:(id)arg1 memberOfClass:(Class)arg2 andEqualToObject:(id)arg3 withBlocks:(/*^block*/id)arg4 ;
+(BOOL)isObject:(id)arg1 memberOfSameClassAndEqualTo:(id)arg2 withBlocks:(/*^block*/id)arg3 ;
+(BOOL)isObject:(id)arg1 equalToOther:(id)arg2 ;
+(BOOL)isBool:(BOOL)arg1 equalToOther:(BOOL)arg2 ;
+(BOOL)isSize:(CGSize)arg1 equalToOther:(CGSize)arg2 ;
@end

