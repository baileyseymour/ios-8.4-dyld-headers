/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:42 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/SpringBoardFoundation.framework/SpringBoardFoundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class SBFWallpaperView;

@interface SBFBackdropStatisticsProvider : NSObject {

	SBFWallpaperView* _wallpaperView;

}

@property (assign) SBFWallpaperView * wallpaperView;              //@synthesize wallpaperView=_wallpaperView - In the implementation block
-(void)backdropLayerStatisticsDidChange:(id)arg1 ;
-(SBFWallpaperView *)wallpaperView;
-(void)setWallpaperView:(SBFWallpaperView *)arg1 ;
@end

