/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:42 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/SpringBoardFoundation.framework/SpringBoardFoundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <SpringBoardFoundation/SBFProceduralWallpaper.h>

@class NSBundle, _UICAPackageView, UIView;

@interface SBFMicaWallpaper : SBFProceduralWallpaper {

	NSBundle* _loadedBundle;
	_UICAPackageView* _packageView;
	UIView* _rootView;

}
+(id)identifier;
+(id)presetWallpaperOptions;
+(id)thumbnailImageForOptions:(id)arg1 ;
-(void)dealloc;
-(void)layoutSubviews;
-(void)setAnimating:(BOOL)arg1 ;
-(void)setWallpaperOptions:(id)arg1 ;
-(void)_unloadView;
-(void)_loadView;
@end

