/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:46 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/FitnessUI.framework/FitnessUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIView.h>

@class FUShapeView;

@interface FUGradientView : UIView {

	FUShapeView* _maskView;

}
+(Class)layerClass;
-(void)setColors:(id)arg1 ;
-(void)setLocations:(id)arg1 ;
-(id)init;
-(void)layoutSubviews;
-(void)setMask:(id)arg1 ;
-(id)gradientLayer;
@end

