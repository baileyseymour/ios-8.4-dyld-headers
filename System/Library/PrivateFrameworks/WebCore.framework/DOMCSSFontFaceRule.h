/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:17 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/WebCore.framework/WebCore
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <WebCore/DOMCSSRule.h>

@class DOMCSSStyleDeclaration;

@interface DOMCSSFontFaceRule : DOMCSSRule

@property (readonly) DOMCSSStyleDeclaration * style; 
-(DOMCSSStyleDeclaration *)style;
@end

