/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:18 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/WebCore.framework/WebCore
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <WebCore/DOMHTMLElement.h>

@class NSString;

@interface DOMHTMLTableCaptionElement : DOMHTMLElement

@property (copy) NSString * align; 
-(NSString *)align;
-(void)setAlign:(NSString *)arg1 ;
@end

