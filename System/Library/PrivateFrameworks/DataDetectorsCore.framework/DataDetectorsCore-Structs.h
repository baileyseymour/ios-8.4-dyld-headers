/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:42 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/DataDetectorsCore.framework/DataDetectorsCore
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

typedef struct __DDScanner* DDScannerRef;

typedef struct NSRange {
	unsigned long long location;
	unsigned long long length;
} NSRange;

typedef struct __CFArray* CFArrayRef;

typedef struct _NSZone* NSZoneRef;

typedef struct {
	long long field1;
	long long field2;
} SCD_Struct_DD4;

