/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:05 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/Symptoms.framework/Frameworks/SymptomEvaluator.framework/SymptomEvaluator
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <SymptomEvaluator/AdditionalInfoProtocol.h>

@class NSString;

@interface ProcessInfoGenerator : NSObject <AdditionalInfoProtocol>

@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
+(id)sharedInstance;
+(id)configure:(id)arg1 ;
+(id)generateAdditionalInfo:(id)arg1 ;
-(int)configure:(id)arg1 ;
-(id)generateAdditionalInfo:(id)arg1 ;
@end

