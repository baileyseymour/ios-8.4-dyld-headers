/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:04 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/Symptoms.framework/Frameworks/SymptomAnalytics.framework/SymptomAnalytics
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol NSCopying
@required
-(id)copyWithZone:(NSZone*)arg1;

@end

