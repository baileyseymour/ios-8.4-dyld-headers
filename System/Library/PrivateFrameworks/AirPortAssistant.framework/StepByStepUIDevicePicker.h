/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:42 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/AirPortAssistant.framework/AirPortAssistant
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol StepByStepUIDevicePicker
@optional
-(void)primaryActionSelected:(BOOL)arg1;
-(void)prepareParams;
-(void)updateNavigationButtons;
-(void)selectedDeviceUpdated;

@required
-(void)setupHeaderAndFooter;
-(void)setupDevices;
-(id)devicePickerLabel;
-(id)deviceTableLabel;

@end

