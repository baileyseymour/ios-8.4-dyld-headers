/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:26 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/CoreHandwriting.framework/CoreHandwriting
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <CoreHandwriting/CHRecognitionResult.h>

@class NSString, NSArray;

@interface CHTextRecognitionResult : CHRecognitionResult {

	BOOL _rare;
	NSString* _string;
	NSArray* _wordRanges;
	NSArray* _wordIDs;

}

@property (readonly) BOOL rare;                         //@synthesize rare=_rare - In the implementation block
@property (readonly) NSString * string;                 //@synthesize string=_string - In the implementation block
@property (readonly) NSArray * wordRanges;              //@synthesize wordRanges=_wordRanges - In the implementation block
@property (readonly) NSArray * wordIDs;                 //@synthesize wordIDs=_wordIDs - In the implementation block
-(void)dealloc;
-(NSString *)string;
-(id)initWithString:(id)arg1 score:(double)arg2 rare:(BOOL)arg3 wordRanges:(id)arg4 wordIDs:(id)arg5 ;
-(BOOL)rare;
-(NSArray *)wordRanges;
-(NSArray *)wordIDs;
@end

