/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:29 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/SAObjects.framework/SAObjects
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <SAObjects/SABaseClientBoundCommand.h>

@class NSString;

@interface SAUIRepeatIt : SABaseClientBoundCommand

@property (nonatomic,copy) NSString * contingency; 
+(id)repeatIt;
+(id)repeatItWithDictionary:(id)arg1 context:(id)arg2 ;
-(BOOL)_afui_isUsefulUserResultCommand;
-(id)groupIdentifier;
-(id)encodedClassName;
-(BOOL)requiresResponse;
-(NSString *)contingency;
-(void)setContingency:(NSString *)arg1 ;
@end

