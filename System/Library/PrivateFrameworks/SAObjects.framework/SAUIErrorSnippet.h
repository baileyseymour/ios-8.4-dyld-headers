/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:26 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/SAObjects.framework/SAObjects
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <SAObjects/SAUISnippet.h>

@class NSString;

@interface SAUIErrorSnippet : SAUISnippet

@property (nonatomic,copy) NSString * message; 
+(id)errorSnippet;
+(id)errorSnippetWithDictionary:(id)arg1 context:(id)arg2 ;
-(id)groupIdentifier;
-(NSString *)message;
-(void)setMessage:(NSString *)arg1 ;
-(id)encodedClassName;
@end

