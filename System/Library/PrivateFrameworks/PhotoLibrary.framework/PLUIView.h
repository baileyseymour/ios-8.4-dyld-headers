/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:07 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/PhotoLibrary.framework/PhotoLibrary
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIView.h>

@interface PLUIView : UIView {

	BOOL _disableViewInPopoverRule;

}

@property (assign,nonatomic) BOOL disableViewInPopoverRule;              //@synthesize disableViewInPopoverRule=_disableViewInPopoverRule - In the implementation block
-(void)didMoveToWindow;
-(BOOL)disableViewInPopoverRule;
-(void)setDisableViewInPopoverRule:(BOOL)arg1 ;
@end

