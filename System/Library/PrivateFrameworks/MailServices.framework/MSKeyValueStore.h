/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:25 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/MailServices.framework/MailServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <MailServices/MSMailDefaultService.h>

@interface MSKeyValueStore : MSMailDefaultService
+(id)valueForKey:(id)arg1 ;
-(void)_simulateServicesMethod:(id)arg1 arguments:(id)arg2 callback:(/*^block*/id)arg3 ;
-(void)_valueForKey:(id)arg1 handler:(/*^block*/id)arg2 ;
@end

