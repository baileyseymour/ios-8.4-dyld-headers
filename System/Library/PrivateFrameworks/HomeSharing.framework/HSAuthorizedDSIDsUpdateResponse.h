/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:13 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/HomeSharing.framework/HomeSharing
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <HomeSharing/HSResponse.h>

@class NSArray;

@interface HSAuthorizedDSIDsUpdateResponse : HSResponse {

	NSArray* authorizedDSIDs;

}

@property (nonatomic,copy) NSArray * authorizedDSIDs; 
-(NSArray *)authorizedDSIDs;
-(void)setAuthorizedDSIDs:(NSArray *)arg1 ;
@end

