/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:40 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/MusicLibrary.framework/MusicLibrary
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol OS_dispatch_queue;
@class ML3LanguageResources, NSObject;

@interface ML3LanguageResourceManager : NSObject {

	ML3LanguageResources* _cachedResources;
	NSObject*<OS_dispatch_queue> _serialQueue;

}
+(id)sharedResourceManager;
-(id)init;
-(void)invalidateCachedResources;
-(void)fetchLanguageResourcesWithCompletion:(/*^block*/id)arg1 ;
-(id)_buildLanguageResources;
-(void)_fetchLanguageResourcesFromMediaLibraryService:(/*^block*/id)arg1 ;
@end

