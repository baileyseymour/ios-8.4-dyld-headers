/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:39 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/SiriUI.framework/SiriUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <SiriUI/SiriUI-Structs.h>
#import <UIKit/UIButton.h>

@interface SiriUIContentButton : UIButton
+(id)button;
+(id)buttonWithImageMask:(id)arg1 ;
+(id)buttonWithLightWeightFont;
+(id)buttonWithMediumWeightFont;
-(id)initWithFrame:(CGRect)arg1 ;
-(void)layoutSubviews;
-(CGSize)sizeThatFits:(CGSize)arg1 ;
-(void)setHighlighted:(BOOL)arg1 ;
@end

