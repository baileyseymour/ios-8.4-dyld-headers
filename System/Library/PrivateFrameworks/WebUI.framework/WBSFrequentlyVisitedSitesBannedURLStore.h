/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:22 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/WebUI.framework/WebUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol OS_dispatch_queue;
@class NSURL, NSObject, NSMutableDictionary;

@interface WBSFrequentlyVisitedSitesBannedURLStore : NSObject {

	NSURL* _storeURL;
	NSObject*<OS_dispatch_queue> _storeQueue;
	NSMutableDictionary* _bannedURLStringsToEntriesMap;

}
-(void)dealloc;
-(void)_historyItemsWereRemoved:(id)arg1 ;
-(void)_historyWasCleared:(id)arg1 ;
-(id)_bannedURLStringsToEntriesMap;
-(void)_writeOutBannedURLStringsAsynchronously;
-(void)removeAllURLStrings;
-(void)removeURLStrings:(id)arg1 ;
-(id)initWithStoreURL:(id)arg1 history:(id)arg2 ;
-(BOOL)containsURLString:(id)arg1 ;
-(void)addURLString:(id)arg1 ;
@end

