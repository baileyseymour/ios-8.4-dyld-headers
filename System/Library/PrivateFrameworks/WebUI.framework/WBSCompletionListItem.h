/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:22 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/WebUI.framework/WebUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

@class NSString;


@protocol WBSCompletionListItem <NSObject>
@property (nonatomic,readonly) NSString * parsecDomainIdentifier; 
@required
-(NSString *)parsecDomainIdentifier;

@end

