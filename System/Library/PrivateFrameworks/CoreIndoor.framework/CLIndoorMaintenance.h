/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:27 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/CoreIndoor.framework/CoreIndoor
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <CoreIndoor/CLIndoorXPCProvider.h>
#import <CoreIndoor/CLIndoorXPCProviderImplementation.h>

@class NSString;

@interface CLIndoorMaintenance : CLIndoorXPCProvider <CLIndoorXPCProviderImplementation>

@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
-(void)shutdown;
-(void)retrieveLocationRelevancyDurationWithCompletionHandler:(/*^block*/id)arg1 ;
-(void)withinQueueInvalidateState;
-(void)doSynchronousXPC:(/*^block*/id)arg1 description:(const char*)arg2 ;
-(id)remoteObjectProtocol;
-(id)endpointName;
-(void)withinQueueReconnectInvalidatedConnectionFailed:(id)arg1 ;
-(id)withinQueuePermanentShutdownReason;
-(BOOL)withinQueueCanReinitializeRemoteState;
-(void)withinQueueReinitializeRemoteState;
-(void)prefetch:(id)arg1 ;
-(void)onQueuePrefetch:(id)arg1 withCallback:(/*^block*/id)arg2 ;
-(void)eraseEverything;
-(void)onQueueEraseEverything:(/*^block*/id)arg1 ;
-(void)onQueueShutdown;
@end

