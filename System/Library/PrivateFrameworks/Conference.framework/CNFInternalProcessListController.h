/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:23 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/Conference.framework/Conference
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <Preferences/PSListController.h>

@interface CNFInternalProcessListController : PSListController
-(id)bundle;
-(void)killJustIChatAgent:(id)arg1 ;
-(void)killJustIMAVAgent:(id)arg1 ;
-(void)killJustMediaServerD:(id)arg1 ;
-(void)conferenceKillProcessesAndNukeKeychain:(id)arg1 ;
-(void)conferenceKillProcesses:(id)arg1 ;
-(id)specifiers;
@end

