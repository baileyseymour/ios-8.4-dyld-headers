/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:29 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/CorePDF.framework/CorePDF
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class CPBody, CPZone;

@interface CPTextBoxMaker : NSObject {

	CPBody* bodyZone;
	CPZone* mainZone;

}
+(void)promoteLayoutsIn:(id)arg1 ;
+(void)boxLayoutsIn:(id)arg1 ;
-(void)rotateTextBox:(id)arg1 ;
-(BOOL)layoutIsSliced:(id)arg1 ;
-(void)promoteLayoutsInCertainRegions:(id)arg1 ;
-(void)promoteLayoutsIn:(id)arg1 ;
-(void)boxLayout:(id)arg1 ;
-(void)boxLayoutsIn:(id)arg1 ;
-(void)makeBoxesWith:(id)arg1 parent:(id)arg2 ;
-(void)rotate:(id)arg1 ;
@end

