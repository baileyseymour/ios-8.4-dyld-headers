/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:32 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/CoreSuggestions.framework/CoreSuggestions
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface SGDRelevantContacts : NSObject
+(id)_recentCallHistory:(long long)arg1 ;
+(id)_recentMessagesHistory:(long long)arg1 ;
+(void)setABRef:(void*)arg1 ;
+(void)relevantABRecordIDsWithLimit:(long long)arg1 completion:(/*^block*/id)arg2 ;
@end

