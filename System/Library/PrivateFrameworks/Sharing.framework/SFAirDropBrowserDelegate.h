/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:37 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/Sharing.framework/Sharing
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol SFAirDropBrowserDelegate <NSObject>
@required
-(void)browserWillChangePeople:(id)arg1;
-(void)browser:(id)arg1 didDeletePersonAtIndex:(unsigned long long)arg2;
-(void)browser:(id)arg1 didInsertPersonAtIndex:(unsigned long long)arg2;
-(void)browserDidChangePeople:(id)arg1;

@end

