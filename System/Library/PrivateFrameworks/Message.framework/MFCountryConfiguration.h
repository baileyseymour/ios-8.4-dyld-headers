/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:35 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/Message.framework/Message
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSConditionLock, NSString;

@interface MFCountryConfiguration : NSObject {

	int _lock;
	NSConditionLock* _generatorLock;
	NSString* _lastCountryCode;
	NSString* _countryCode;

}

@property (nonatomic,copy,readonly) NSString * countryCode; 
+(id)sharedConfiguration;
-(void)dealloc;
-(id)init;
-(void)invalidate;
-(id)_cellSimCountryCode;
-(id)_networkCountryCode;
-(id)_countryCodeWithGenerator:(/*^block*/id)arg1 ;
-(void)updateCurrentCountry;
-(NSString *)countryCode;
@end

