/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:34 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/Message.framework/Message
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class MFMessageLibrary, NSString;

@interface MFMessageDetails : NSObject {

	MFMessageLibrary* library;
	unsigned libraryID;
	unsigned long long messageFlags;
	unsigned uid;
	unsigned encoding;
	BOOL isInvalid;
	unsigned mailboxID;
	long long conversationHash;
	long long messageIDHash;
	unsigned dateReceived;
	unsigned dateSent;
	NSString* externalID;

}
-(id)externalID;
-(unsigned)libraryID;
-(void)dealloc;
-(id)init;
-(BOOL)isEqual:(id)arg1 ;
-(unsigned long long)hash;
-(id)description;
-(unsigned)uid;
-(unsigned long long)messageFlags;
-(double)dateReceivedAsTimeIntervalSince1970;
-(double)dateSentAsTimeIntervalSince1970;
-(unsigned)mailboxID;
-(id)messageID;
-(id)remoteID;
-(id)mailbox;
-(id)copyMessageInfo;
-(long long)messageIDHash;
@end

