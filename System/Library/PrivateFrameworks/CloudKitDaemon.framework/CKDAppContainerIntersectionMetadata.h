/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:19 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/CloudKitDaemon.framework/CloudKitDaemon
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface CKDAppContainerIntersectionMetadata : NSObject {

	long long _tokenRegistered;
	long long _usesAPSPublicToken;
	long long _darkWakeEnabled;

}

@property (assign,nonatomic) long long tokenRegistered;                 //@synthesize tokenRegistered=_tokenRegistered - In the implementation block
@property (assign,nonatomic) long long usesAPSPublicToken;              //@synthesize usesAPSPublicToken=_usesAPSPublicToken - In the implementation block
@property (assign,nonatomic) long long darkWakeEnabled;                 //@synthesize darkWakeEnabled=_darkWakeEnabled - In the implementation block
-(id)init;
-(void)setUsesAPSPublicToken:(long long)arg1 ;
-(void)setDarkWakeEnabled:(long long)arg1 ;
-(long long)usesAPSPublicToken;
-(long long)darkWakeEnabled;
-(long long)tokenRegistered;
-(void)setTokenRegistered:(long long)arg1 ;
@end

