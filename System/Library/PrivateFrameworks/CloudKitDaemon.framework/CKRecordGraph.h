/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:16 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/CloudKitDaemon.framework/CloudKitDaemon
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSMutableSet, NSArray;

@interface CKRecordGraph : NSObject {

	NSMutableSet* _nodes;
	NSArray* _sortedRecords;

}

@property (nonatomic,copy) NSMutableSet * nodes;                   //@synthesize nodes=_nodes - In the implementation block
@property (nonatomic,retain) NSArray * sortedRecords;              //@synthesize sortedRecords=_sortedRecords - In the implementation block
-(id)init;
-(id)description;
-(void)setSortedRecords:(NSArray *)arg1 ;
-(NSArray *)sortedRecords;
-(BOOL)addRecords:(id)arg1 error:(id*)arg2 ;
-(id)recordsByTopologicalSortWithError:(id*)arg1 ;
-(void)setNodes:(NSMutableSet *)arg1 ;
-(NSMutableSet *)nodes;
@end

