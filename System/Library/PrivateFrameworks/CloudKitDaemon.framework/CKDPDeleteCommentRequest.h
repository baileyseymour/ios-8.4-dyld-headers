/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:15 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/CloudKitDaemon.framework/CloudKitDaemon
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <CloudKitDaemon/CloudKitDaemon-Structs.h>
#import <ProtocolBuffer/PBRequest.h>
#import <CloudKitDaemon/NSCopying.h>

@class CKDPIdentifier;

@interface CKDPDeleteCommentRequest : PBRequest <NSCopying> {

	CKDPIdentifier* _commentId;

}

@property (nonatomic,readonly) BOOL hasCommentId; 
@property (nonatomic,retain) CKDPIdentifier * commentId;              //@synthesize commentId=_commentId - In the implementation block
+(id)options;
-(BOOL)isEqual:(id)arg1 ;
-(unsigned long long)hash;
-(id)description;
-(id)copyWithZone:(NSZone*)arg1 ;
-(id)dictionaryRepresentation;
-(unsigned)requestTypeCode;
-(Class)responseClass;
-(void)setCommentId:(CKDPIdentifier *)arg1 ;
-(BOOL)hasCommentId;
-(CKDPIdentifier *)commentId;
-(void)mergeFrom:(id)arg1 ;
-(BOOL)readFrom:(id)arg1 ;
-(void)writeTo:(id)arg1 ;
-(void)copyTo:(id)arg1 ;
@end

