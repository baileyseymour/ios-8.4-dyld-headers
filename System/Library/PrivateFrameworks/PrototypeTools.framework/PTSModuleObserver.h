/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:19 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/PrototypeTools.framework/PrototypeTools
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol PTSModuleObserver <NSObject>
@required
-(void)module:(id)arg1 didInsertRows:(id)arg2 deleteRows:(id)arg3;
-(void)moduleDidReload:(id)arg1;
-(void)module:(id)arg1 didInsertSections:(id)arg2 deleteSections:(id)arg3;

@end

