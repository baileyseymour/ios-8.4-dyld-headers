/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:19 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/PrototypeTools.framework/PrototypeTools
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <PrototypeTools/PrototypeTools-Structs.h>
#import <PrototypeTools/_UISettingsKeyObserver.h>

@class NSHashTable, CMMotionManager, CADisplayLink, PTXParallaxEngineSettings, PTXParallaxReferenceFrame, NSMutableSet, NSString;

@interface PTXParallaxController : NSObject <_UISettingsKeyObserver> {

	NSHashTable* _observers;
	CMMotionManager* _motionManager;
	CADisplayLink* _displayLink;
	PTXParallaxEngineSettings* _settings;
	PTXParallaxReferenceFrame* _referenceFrame;
	double _startUpdatesTimestamp;
	BOOL _generatingUpdates;
	NSMutableSet* _suspendReasons;

}

@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
-(void)dealloc;
-(id)init;
-(void)stop;
-(void)removeObserver:(id)arg1 ;
-(void)start;
-(void)settings:(id)arg1 changedValueForKey:(id)arg2 ;
-(void)_startOrStopGeneratingUpdates;
-(void)_stopGeneratingUpdates;
-(BOOL)_shouldGenerateUpdates;
-(void)_startGeneratingUpdates;
-(void)_tearDownDisplayLink;
-(void)addObserver:(id)arg1 ;
-(void)resume:(id)arg1 ;
-(id)initWithParallaxEngineSettings:(id)arg1 ;
-(void)_setUpDisplayLink;
-(void)_sendClearOffset;
-(void)_onDisplayLink:(id)arg1 ;
-(void)_sendOffset:(CGPoint)arg1 lockStatus:(long long)arg2 lockStrength:(double)arg3 ;
-(BOOL)_suspended;
-(void)suspend:(id)arg1 ;
-(void)resetReferenceFrame;
-(void)_defaultsChanged:(id)arg1 ;
@end

