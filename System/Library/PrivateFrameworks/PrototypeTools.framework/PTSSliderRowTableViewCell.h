/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:19 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/PrototypeTools.framework/PrototypeTools
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <PrototypeTools/PTSRowTableViewCell.h>

@class UISlider;

@interface PTSSliderRowTableViewCell : PTSRowTableViewCell {

	UISlider* _slider;

}
+(double)cellHeightForRow:(id)arg1 ;
-(void)layoutSubviews;
-(id)initWithStyle:(long long)arg1 reuseIdentifier:(id)arg2 ;
-(void)updateCellCharacteristics;
-(void)updateDisplayedValue;
-(void)_valueChanged:(id)arg1 ;
@end

