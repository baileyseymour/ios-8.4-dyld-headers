/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:15 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/IMAVCore.framework/IMAVCore
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol OS_dispatch_queue;
@class NSMutableArray, NSObject, NSArray, IMAVMicrophone;

@interface IMAVMicrophoneController : NSObject {

	id _internal;
	NSMutableArray* _microphones;
	NSObject*<OS_dispatch_queue> _queue;

}

@property (nonatomic,retain,readonly) NSArray * microphones; 
@property (nonatomic,retain) IMAVMicrophone * currentMicrophone; 
+(id)sharedInstance;
-(void)dealloc;
-(id)init;
-(NSArray *)microphones;
-(void)setCurrentMicrophone:(IMAVMicrophone *)arg1 ;
-(void)_rebuildMicrophoneList;
-(void)_loadSavedMicrophone;
-(IMAVMicrophone *)currentMicrophone;
@end

