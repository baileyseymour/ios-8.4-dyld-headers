/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:12 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/CloudDocsDaemon.framework/CloudDocsDaemon
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <CoreFoundation/NSEnumerator.h>

@class NSMapTable, NSMutableArray;

@interface BRCRRQueue : NSEnumerator {

	NSMapTable* _objects;
	NSMutableArray* _array;
	unsigned long long _pos;

}

@property (nonatomic,readonly) unsigned long long count; 
-(void)removeObject:(id)arg1 ;
-(unsigned long long)count;
-(id)init;
-(void)addObject:(id)arg1 ;
-(void)removeAllObjects;
-(BOOL)containsObject:(id)arg1 ;
-(id)initWithCapacity:(unsigned long long)arg1 ;
-(id)nextObject;
@end

