/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:24 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/MPUFoundation.framework/MPUFoundation
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol _UIDynamicReferenceSystem <NSObject>
@optional
-(CGPoint*)convertPoint:(CGPoint)arg1 fromView:(id)arg2;
-(CGPoint*)convertPoint:(CGPoint)arg1 toView:(id)arg2;
-(CGRect*)_dynamicReferenceBounds;

@required
-(CGRect*)bounds;

@end

