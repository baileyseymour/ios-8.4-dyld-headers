/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:37 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/Search.framework/Search
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSString, NSMutableString, NSNumber;

@interface SPPhoneticEncoder : NSObject {

	NSString* _source;
	NSMutableString* _primary;
	NSMutableString* _alternate;
	int _idx;
	NSNumber* _slavoGermanic;

}
-(BOOL)isAnyString:(id)arg1 atIndex:(int)arg2 ;
-(BOOL)isVowelAtIndex:(int)arg1 ;
-(void)addPrimary:(id)arg1 alternate:(id)arg2 ;
-(BOOL)isSlavoGermanic;
-(id)codesForString:(id)arg1 ;
-(void)add:(id)arg1 ;
@end

