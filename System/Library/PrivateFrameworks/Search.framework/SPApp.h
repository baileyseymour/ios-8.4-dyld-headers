/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:37 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/Search.framework/Search
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <Search/SPProxyObject.h>
#import <Search/PRSApp.h>

@class NSString, NSURL;

@interface SPApp : SPProxyObject <PRSApp>

@property (nonatomic,retain) NSString * name; 
@property (nonatomic,retain) NSString * bundle_id; 
@property (nonatomic,retain) NSURL * install_url; 
@property (nonatomic,retain) NSURL * punchout_uri; 
@end

