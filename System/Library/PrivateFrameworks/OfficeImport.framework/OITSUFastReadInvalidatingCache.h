/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:49 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSArray, NSCondition;

@interface OITSUFastReadInvalidatingCache : NSObject {

	/*^block*/id mGenerator;
	id mValue;
	long long mReaderCount;
	NSArray* mToDispose;
	BOOL mReentrant;
	NSCondition* mCondition;
	BOOL mIsGenerating;

}
-(void)dealloc;
-(void)invalidate;
-(id)value;
-(void)p_setValue:(id)arg1 ;
-(id)initForReentrant:(BOOL)arg1 withGenerator:(/*^block*/id)arg2 ;
@end

