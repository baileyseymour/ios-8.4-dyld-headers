/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:00 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <OfficeImport/OfficeImport-Structs.h>
@interface ECColumnWidthConvertor : NSObject {

	int mMultiplier;

}
-(CGSize)stringSizeWithFont:(id)arg1 edFont:(id)arg2 drawingState:(id)arg3 ;
-(double)lassoColumnWidthFromXl:(double)arg1 ;
-(void)setupWithEDFont:(id)arg1 state:(id)arg2 ;
-(double)xlColumnWidthFromXlBaseColumnWidth:(double)arg1 ;
-(double)xlColumnWidthFromLasso:(double)arg1 ;
-(double)xlBaseColumnWidthFromXlColumnWidth:(double)arg1 ;
@end

