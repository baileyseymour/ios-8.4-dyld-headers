/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:56 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <OfficeImport/CHDChartType.h>

@interface CHDChartTypeWithGrouping : CHDChartType {

	int mGrouping;

}
-(void)setGrouping:(int)arg1 ;
-(int)grouping;
-(id)initWithChart:(id)arg1 ;
-(BOOL)isGroupingStacked;
-(id)chdGroupingString;
@end

