/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:55 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <OfficeImport/MFObject.h>

@interface MFRegion : NSObject <MFObject>
-(int)selectInto:(id)arg1 ;
-(int)setClip:(id)arg1 :(int)arg2 ;
-(int)fill:(id)arg1 :(id)arg2 ;
-(int)frame:(id)arg1 :(id)arg2 ;
-(int)invert:(id)arg1 ;
@end

