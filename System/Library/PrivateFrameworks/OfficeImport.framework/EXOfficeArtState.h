/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:00 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <OfficeImport/OAXDrawingState.h>

@class EXReadState;

@interface EXOfficeArtState : OAXDrawingState {

	EXReadState* mExcelState;

}
-(id)excelState;
-(id)initWithExcelState:(id)arg1 ;
@end

