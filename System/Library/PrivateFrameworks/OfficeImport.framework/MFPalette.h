/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:55 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <OfficeImport/MFObject.h>

@class NSMutableArray;

@interface MFPalette : NSObject <MFObject> {

	NSMutableArray* m_colours;

}
+(id)paletteWithColours:(id)arg1 ;
-(void)dealloc;
-(id)init;
-(int)selectInto:(id)arg1 ;
-(id)initWithColours:(id)arg1 ;
-(BOOL)setEntries:(int)arg1 :(id)arg2 ;
-(BOOL)resize:(int)arg1 ;
-(id)getColour:(int)arg1 ;
@end

