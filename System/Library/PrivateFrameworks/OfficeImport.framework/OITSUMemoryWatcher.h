/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:48 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class OITSUFlushingManager;

@interface OITSUMemoryWatcher : NSObject {

	OITSUFlushingManager* _flushingManager;
	BOOL _going;
	BOOL _stop;

}
-(void)dealloc;
-(id)initWithFlushingManager:(id)arg1 ;
-(void)stopObserving;
-(void)_periodicallySimulateMemoryWarning:(id)arg1 ;
-(void)_simulateMemoryWarning:(id)arg1 ;
-(void)beginObserving;
@end

