/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:48 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <OfficeImport/OfficeImport-Structs.h>
#import <CoreFoundation/NSSet.h>

@interface OITSUPointerSet : NSSet {

	CFSetRef mSet;

}
+(Class)privateMutableClass;
+(Class)privateNonMutableClass;
-(void)getObjects:(id*)arg1 ;
-(void)dealloc;
-(unsigned long long)count;
-(id)allObjects;
-(id)copyWithZone:(NSZone*)arg1 ;
-(id)objectEnumerator;
-(id)setByAddingObjectsFromSet:(id)arg1 ;
-(id)initWithObjects:(const id*)arg1 count:(unsigned long long)arg2 ;
-(id)mutableCopyWithZone:(NSZone*)arg1 ;
-(id)member:(id)arg1 ;
-(id)setByAddingObjectsFromArray:(id)arg1 ;
-(id)initWithCFSet:(CFSetRef)arg1 ;
-(id)setByAddingObject:(id)arg1 ;
@end

