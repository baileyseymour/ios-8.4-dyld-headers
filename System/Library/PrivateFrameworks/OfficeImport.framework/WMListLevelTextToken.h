/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:46 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSString;

@interface WMListLevelTextToken : NSObject {

	NSString* m_string;
	int m_level;

}
+(id)tokenWithString:(id)arg1 andLevel:(int)arg2 ;
-(void)dealloc;
-(id)string;
-(int)level;
-(id)stringForIndex:(unsigned long long)arg1 withFormat:(int)arg2 orNumberFormatter:(void*)arg3 initialNumber:(unsigned long long)arg4 ;
-(id)initWithString:(id)arg1 andLevel:(int)arg2 ;
@end

