/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:55 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <OfficeImport/OfficeImport-Structs.h>
@class NSString;

@interface STStgInfo : NSObject {

	NSString* m_pstrName;
	int m_type;
	unsigned m_userFlags;
	unsigned m_size;
	long long m_creationTime;
	long long m_modificationTime;
	int m_accessMode;
	SCD_Struct_ST69 m_clsid;

}
-(void)dealloc;
-(id)init;
-(void)setName:(id)arg1 ;
-(void)setType:(int)arg1 ;
-(void)setSize:(unsigned)arg1 ;
-(void)setCLSID:(SCD_Struct_ST69)arg1 ;
-(id)getName;
-(int)getType;
-(unsigned)getUserFlags;
-(void)setUserFlags:(unsigned)arg1 ;
-(unsigned)getSize;
-(long long)getCreationTime;
-(void)setCreationTime:(long long)arg1 ;
-(long long)getModificationTime;
-(void)setModificationTime:(long long)arg1 ;
-(int)getAccessMode;
-(void)setAccessMode:(int)arg1 ;
-(SCD_Struct_ST69)getCLSID;
-(id)initWithStgInfo:(StgInfo*)arg1 ;
@end

