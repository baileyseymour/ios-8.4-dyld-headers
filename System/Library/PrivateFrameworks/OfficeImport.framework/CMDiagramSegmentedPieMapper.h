/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:47 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <OfficeImport/OfficeImport-Structs.h>
#import <OfficeImport/CMDiagramShapeMapper.h>

@interface CMDiagramSegmentedPieMapper : CMDiagramShapeMapper {

	BOOL mDrawArrows;

}
-(void)mapAt:(id)arg1 withState:(id)arg2 ;
-(void)setDrawArrows:(BOOL)arg1 ;
-(void)mapChildrenAt:(id)arg1 withState:(id)arg2 ;
-(CGSize)sizeForNode:(id)arg1 atIndex:(unsigned long long)arg2 ;
-(id)_suggestedBoundsForNodeAtIndex:(unsigned long long)arg1 ;
-(int)pointCount;
@end

