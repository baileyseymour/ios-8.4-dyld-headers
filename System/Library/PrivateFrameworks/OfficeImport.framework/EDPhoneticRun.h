/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:59 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface EDPhoneticRun : NSObject {

	unsigned mCharIndex;
	unsigned mBaseCharIndex;
	unsigned mBaseCharCount;

}
-(id)init;
-(unsigned)charIndex;
-(unsigned)charBaseIndex;
-(unsigned)charBaseCount;
-(void)setCharIndex:(unsigned)arg1 ;
-(void)setCharBaseIndex:(unsigned)arg1 ;
-(void)setCharBaseCount:(unsigned)arg1 ;
-(void)adjustIndex:(unsigned long long)arg1 ;
@end

