/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:04:58 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <OfficeImport/OfficeImport-Structs.h>
@interface EBPane : NSObject
+(void)writePaneForSheet:(id)arg1 toXlSheetPresentation:(XlSheetPresentation*)arg2 xlWindow2:(XlWindow2*)arg3 ;
+(void)readXlPaneFrom:(XlSheetPresentation*)arg1 state:(id)arg2 ;
@end

