/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:00 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/OfficeImport.framework/OfficeImport
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface WBEscher : NSObject
+(void)readRootContainer:(id*)arg1 type:(int*)arg2 reader:(id)arg3 ;
+(id)readRootWithType:(int*)arg1 reader:(id)arg2 ;
@end

