/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:59 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/CameraKit.framework/CameraKit
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <CameraKit/CAMImagePickerSpec.h>

@interface CAMPadImagePickerSpec : CAMImagePickerSpec
-(BOOL)shouldCreateFlashButton;
-(BOOL)shouldCreateElapsedTimeView;
-(BOOL)shouldCreateFlipButton;
-(BOOL)shouldCreateStillDuringVideo;
-(BOOL)shouldCreateImageWell;
-(BOOL)shouldCreateShutterButton;
-(BOOL)shouldCreateFiltersButton;
-(BOOL)shouldCreateModeDial;
-(BOOL)shouldCreateHDRButton;
-(BOOL)shouldCreatePanoramaView;
-(BOOL)shouldCreateZoomSlider;
@end

