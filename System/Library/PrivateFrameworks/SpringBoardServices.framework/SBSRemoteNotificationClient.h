/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:05:43 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/SpringBoardServices.framework/SpringBoardServices
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface SBSRemoteNotificationClient : NSObject
+(void)getSupportedBundleIdentifiers:(id*)arg1 enabledBundleIdentifiers:(id*)arg2 ;
+(void)setAllowed:(BOOL)arg1 forBundleIdentifier:(id)arg2 ;
+(unsigned)_remoteNotificationServerPort;
+(void)registerForRemoteNotifications;
+(BOOL)isRegisteredForRemoteNotifications;
+(void)unregisterForRemoteNotifications;
+(unsigned char)legacyRegisteredRemoteNotificationTypes;
+(void)registerForRemoteNotificationsWithLegacyTypes:(unsigned char)arg1 ;
+(id)getPendingNotificationUserInfo;
+(id)getNotificationUserInfoForToken:(int)arg1 ;
@end

