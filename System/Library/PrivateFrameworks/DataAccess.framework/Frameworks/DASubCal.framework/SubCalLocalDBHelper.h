/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:41 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/DataAccess.framework/Frameworks/DASubCal.framework/DASubCal
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <DataAccess/DALocalDBHelper.h>

@interface SubCalLocalDBHelper : DALocalDBHelper
+(id)sharedInstance;
-(void*)initializedStoreCopyWithExternalId:(id)arg1 ;
-(void*)initializedCalendarCopyWithExternalId:(id)arg1 inStore:(void*)arg2 needsSave:(BOOL*)arg3 ;
@end

