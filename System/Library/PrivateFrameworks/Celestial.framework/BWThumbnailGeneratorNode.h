/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:03 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/Celestial.framework/Celestial
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <Celestial/Celestial-Structs.h>
#import <Celestial/BWNode.h>

@interface BWThumbnailGeneratorNode : BWNode {

	OpaqueVTPixelTransferSessionRef _pixelTransferSession;
	BOOL _rotateIfPortrait;
	int _rotationDegrees;
	OpaqueVTImageRotationSessionRef _imageRotationSession;

}
+(void)initialize;
-(void)dealloc;
-(id)nodeType;
-(id)nodeSubType;
-(void)renderSampleBuffer:(opaqueCMSampleBufferRef)arg1 forInput:(id)arg2 ;
-(void)didSelectFormat:(id)arg1 forInput:(id)arg2 ;
-(void)prepareForCurrentConfigurationToBecomeLive;
-(id)initWithRotationHint:(BOOL)arg1 rotationDegrees:(int)arg2 ;
@end

