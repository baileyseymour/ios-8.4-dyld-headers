/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:03:44 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PrivateFrameworks/FMF.framework/FMF
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@interface FMFLabelUtil : NSObject
+(id)defaultLabelKeys;
+(BOOL)isDefaultLabel:(id)arg1 ;
@end

