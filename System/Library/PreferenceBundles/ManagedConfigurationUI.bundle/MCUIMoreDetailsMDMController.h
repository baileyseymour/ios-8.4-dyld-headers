/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:34 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PreferenceBundles/ManagedConfigurationUI.bundle/ManagedConfigurationUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <Preferences/PSListController.h>

@class MCDetailsDescriptionTableCell;

@interface MCUIMoreDetailsMDMController : PSListController {

	MCDetailsDescriptionTableCell* _descCell;

}
-(double)tableView:(id)arg1 heightForRowAtIndexPath:(id)arg2 ;
-(id)tableView:(id)arg1 cellForRowAtIndexPath:(id)arg2 ;
-(id)specifiers;
@end

