/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:33 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PreferenceBundles/ManagedConfigurationUI.bundle/ManagedConfigurationUI
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@protocol PSStateRestoration <NSObject>
@required
-(BOOL)canBeShownFromSuspendedState;

@end

