/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:31 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PreferenceBundles/AccountSettings/MobileMailSettings.bundle/MobileMailSettings
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <MobileMailSettings/MobileMailSettings-Structs.h>
@interface MessageToMailboxUidCache : NSObject {

	CFDictionaryRef _mailboxCache;

}
-(void)dealloc;
-(unsigned long long)count;
-(id)init;
-(void)removeAllObjects;
-(id)mailboxForMessage:(id)arg1 ;
@end

