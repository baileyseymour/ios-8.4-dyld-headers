/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:31 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PreferenceBundles/AirPortSettings.bundle/AirPortSettings
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <Preferences/PSSetupController.h>

@interface APOtherNetworkSetup : PSSetupController
-(void)navigationBar:(id)arg1 buttonClicked:(int)arg2 ;
-(void)dismiss;
-(void)_popToIndex:(int)arg1 ;
-(void)updateNavButtons;
-(void)updateNavigationBarButtonsAfterControllerPush;
-(void)_pop;
-(BOOL)canBeShownFromSuspendedState;
@end

