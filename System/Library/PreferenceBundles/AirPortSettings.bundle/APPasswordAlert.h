/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:31 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PreferenceBundles/AirPortSettings.bundle/AirPortSettings
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIModalViewDelegate.h>

@class WiFiNetwork, UIAlertView, NSString;

@interface APPasswordAlert : NSObject <UIModalViewDelegate> {

	id _delegate;
	WiFiNetwork* _network;
	UIAlertView* _sheet;

}

@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
-(void)_configure;
-(void)dealloc;
-(id)init;
-(void)dismiss;
-(void)showAlertForNetwork:(id)arg1 delegate:(id)arg2 ;
-(id)getNetwork;
-(BOOL)isValidPassword;
@end

