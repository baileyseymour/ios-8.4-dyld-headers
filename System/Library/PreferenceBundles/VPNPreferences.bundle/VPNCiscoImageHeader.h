/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:02:35 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /System/Library/PreferenceBundles/VPNPreferences.bundle/VPNPreferences
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIView.h>
#import <VPNPreferences/PSHeaderFooterView.h>

@class UIImageView;

@interface VPNCiscoImageHeader : UIView <PSHeaderFooterView> {

	UIImageView* _imageView;

}
-(id)initWithSpecifier:(id)arg1 ;
-(void)setImageHidden:(BOOL)arg1 ;
-(double)preferredHeightForWidth:(double)arg1 ;
-(void)layoutSubviews;
@end

