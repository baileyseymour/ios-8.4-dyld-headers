/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:44 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /usr/lib/libGestureServer.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <libGestureServer.dylib/NSXPCListenerDelegate.h>
#import <libGestureServer.dylib/_UIGestureRecognizerServerInterface.h>

@protocol OS_dispatch_queue;
@class NSMutableSet, NSMapTable, NSMutableDictionary, NSXPCListener, NSObject, NSString;

@interface _UIGestureRecognizerServer : NSObject <NSXPCListenerDelegate, _UIGestureRecognizerServerInterface> {

	NSMutableSet* _contextReuseQueue;
	NSMapTable* _contextToStateMap;
	NSMutableDictionary* _handEventToHitContextsMap;
	NSXPCListener* _listener;
	NSObject*<OS_dispatch_queue> _handlerQueue;

}

@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
+(id)sharedServer;
-(void)dealloc;
-(id)init;
-(void)run;
-(void)ipc_setGestureRecognizerState:(long long)arg1 forGestureRecognizersInContextId:(unsigned)arg2 withInitialTouchTimestamp:(double)arg3 lastTouchTimestamp:(double)arg4 ;
-(void)ipc_setGestureRecognizerState:(long long)arg1 forGestureRecognizersInContextId:(unsigned)arg2 affectingContextId:(unsigned)arg3 withInitialTouchTimestamp:(double)arg4 lastTouchTimestamp:(double)arg5 ;
-(void)ipc_removeGestureRecognizerStateForContextId:(unsigned)arg1 withInitialTouchTimestamp:(double)arg2 ;
-(void)ipc_requestGestureRecognizerInstructionForContextID:(unsigned)arg1 withInitialTouchTimestamp:(double)arg2 reply:(/*^block*/id)arg3 ;
-(BOOL)listener:(id)arg1 shouldAcceptNewConnection:(id)arg2 ;
-(id)_queue_stateForContextId:(unsigned)arg1 withInitialTouchTimestamp:(double)arg2 createIfMissing:(BOOL)arg3 ;
-(long long)_queue_touchDeliveryStateForContextId:(unsigned)arg1 withInitialTouchTimestamp:(double)arg2 ;
-(void)_queue_removeGestureRecognizerStateForContextId:(unsigned)arg1 withInitialTouchTimestamp:(double)arg2 ;
-(void)_queue_enumerateContextStatesWithBlock:(/*^block*/id)arg1 ;
-(long long)_queue_gestureRecognizersCanBeginGivenDescendantsOfContextId:(unsigned)arg1 withInitialTouchTimestamp:(double)arg2 ancestorGestureRecognizerState:(long long)arg3 lastTouchTimestamp:(double)arg4 ;
-(long long)_queue_gestureRecognizersCanBeginGivenAncestorsOfContextId:(unsigned)arg1 withInitialTouchTimestamp:(double)arg2 ;
-(void)_queue_touchesWithInitialTouchTimestamp:(double)arg1 wereDeliveredToContexts:(id)arg2 ;
-(void)touchesWithInitialTouchTimestamp:(double)arg1 wereDeliveredToContexts:(unsigned*)arg2 count:(unsigned char)arg3 ;
@end

