/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:48 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /usr/lib/libobjc.A.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <libobjc.A.dylib/libobjc.A.dylib-Structs.h>
@interface Protocol : NSObject
+(void)load;
-(BOOL)isEqual:(id)arg1 ;
-(unsigned long long)hash;
-(const char*)name;
-(BOOL)conformsTo:(id)arg1 ;
-(objc_method_description*)descriptionForInstanceMethod:(SEL)arg1 ;
-(objc_method_description*)descriptionForClassMethod:(SEL)arg1 ;
@end

