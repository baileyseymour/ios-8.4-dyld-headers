/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:47 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /usr/lib/libmecabra.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


#import <libmecabra.dylib/libmecabra.dylib-Structs.h>
@interface MecabraDatabaseListener : NSObject {

	LearningDictionary* target;

}
-(void)dealloc;
-(void)resetDatabase;
-(id)initWithLearningDictionary:(LearningDictionary*)arg1 ;
@end

