/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:48 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /usr/lib/libnfshared.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <libnfshared.dylib/NFAWDEventProtocol.h>

@class NSData, AWDNFCJCOPRestrictedModeEvent, NSString;

@interface NFAWDRestrictedModeEnter : NSObject <NFAWDEventProtocol> {

	BOOL contactlessMode;
	NSData* attackLog;
	AWDNFCJCOPRestrictedModeEvent* _metric;

}

@property (assign,nonatomic) BOOL contactlessMode; 
@property (nonatomic,retain) NSData * attackLog; 
@property (assign) AWDNFCJCOPRestrictedModeEvent * metric;              //@synthesize metric=_metric - In the implementation block
@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
-(void)dealloc;
-(id)init;
-(AWDNFCJCOPRestrictedModeEvent *)metric;
-(void)setMetric:(AWDNFCJCOPRestrictedModeEvent *)arg1 ;
-(unsigned)getMetricId;
-(id)getMetric;
-(void)setContactlessMode:(BOOL)arg1 ;
-(void)setAttackLog:(NSData *)arg1 ;
-(BOOL)contactlessMode;
-(NSData *)attackLog;
@end

