/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:48 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /usr/lib/libnfshared.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <libnfshared.dylib/NFAWDEventProtocol.h>

@class AWDNFCSECRSAuthInit, NSString;

@interface NFAWDCRSAuthInit : NSObject <NFAWDEventProtocol> {

	unsigned status;
	AWDNFCSECRSAuthInit* _metric;

}

@property (assign,nonatomic) unsigned status; 
@property (assign) AWDNFCSECRSAuthInit * metric;                    //@synthesize metric=_metric - In the implementation block
@property (readonly) unsigned long long hash; 
@property (readonly) Class superclass; 
@property (copy,readonly) NSString * description; 
@property (copy,readonly) NSString * debugDescription; 
-(void)dealloc;
-(id)init;
-(unsigned)status;
-(void)setStatus:(unsigned)arg1 ;
-(AWDNFCSECRSAuthInit *)metric;
-(void)setMetric:(AWDNFCSECRSAuthInit *)arg1 ;
-(unsigned long long)updateTransactionStateInfoPreviousState:(unsigned long long)arg1 withUUID:(id)arg2 withUUIDRefTimestamp:(unsigned long long)arg3 ;
-(unsigned)getMetricId;
-(id)getMetric;
@end

