/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:46 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /usr/lib/libacmobileshim.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/


@class NSDictionary;

@interface ACMBaseLocale : NSObject {

	NSDictionary* _localeStrings;
	ACMBaseLocale* _nextLocale;

}

@property (retain) ACMBaseLocale * nextLocale;              //@synthesize nextLocale=_nextLocale - In the implementation block
+(id)sharedInstance;
+(id)localizedString:(id)arg1 ;
+(id)currentLanguageCode;
+(void)initResources;
+(id)localeForIdentifier:(id)arg1 ;
+(id)localeForLanguage:(id)arg1 ;
-(void)dealloc;
-(id)objectForKey:(id)arg1 ;
-(void)setNextLocale:(ACMBaseLocale *)arg1 ;
-(ACMBaseLocale *)nextLocale;
@end

