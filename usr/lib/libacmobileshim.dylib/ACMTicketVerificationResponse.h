/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:45 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /usr/lib/libacmobileshim.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

@class NSNumber, NSDictionary, NSError;


@protocol ACMTicketVerificationResponse <NSObject>
@property (retain,readonly) NSNumber * personDSID; 
@property (retain,readonly) NSDictionary * rawResponseData; 
@property (retain,readonly) NSError * error; 
@property (retain,readonly) id userInfo; 
@required
-(id)userInfo;
-(NSError *)error;
-(NSDictionary *)rawResponseData;
-(NSNumber *)personDSID;

@end

