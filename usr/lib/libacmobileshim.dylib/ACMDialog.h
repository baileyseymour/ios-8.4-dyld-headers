/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:45 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /usr/lib/libacmobileshim.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <UIKit/UIViewController.h>

@class NSString;

@interface ACMDialog : UIViewController {

	long long _savedStatusBarStyle;

}

@property (readonly) long long statusBarStyle; 
@property (assign) long long savedStatusBarStyle;                 //@synthesize savedStatusBarStyle=_savedStatusBarStyle - In the implementation block
@property (readonly) NSString * backgroundImageName; 
+(id)dialog;
+(void)setDialogOnFlight:(BOOL)arg1 ;
+(BOOL)dialogOnFlight;
-(void)dealloc;
-(id)init;
-(long long)statusBarStyle;
-(void)loadView;
-(void)viewWillAppear:(BOOL)arg1 ;
-(void)viewDidAppear:(BOOL)arg1 ;
-(void)viewWillDisappear:(BOOL)arg1 ;
-(void)viewDidDisappear:(BOOL)arg1 ;
-(NSString *)backgroundImageName;
-(void)applicationDidBecomeActive;
-(void)applicationWillResignActive;
-(void)applicationDidEnterBackground;
-(void)applicationWillEnterForeground;
-(void)disableControls:(BOOL)arg1 ;
-(void)showWithOptions:(id)arg1 ;
-(void)hideWithOptions:(id)arg1 ;
-(void)setSavedStatusBarStyle:(long long)arg1 ;
-(long long)savedStatusBarStyle;
-(void)showWithParentViewController:(id)arg1 animated:(BOOL)arg2 ;
-(void)hideWithParentViewController:(id)arg1 animated:(BOOL)arg2 ;
@end

