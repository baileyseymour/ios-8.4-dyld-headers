/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:47 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /usr/lib/libextension.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <libextension.dylib/libextension.dylib-Structs.h>
#import <libextension.dylib/NSCopying.h>
#import <libextension.dylib/NSSecureCoding.h>

@class NSMutableDictionary, NSAttributedString, NSArray, NSDictionary;

@interface NSExtensionItem : NSObject <NSCopying, NSSecureCoding> {

	NSMutableDictionary* _userInfo;

}

@property (nonatomic,copy) NSAttributedString * attributedTitle; 
@property (nonatomic,copy) NSAttributedString * attributedContentText; 
@property (nonatomic,copy) NSArray * attachments; 
@property (nonatomic,copy) NSDictionary * userInfo; 
+(BOOL)supportsSecureCoding;
-(NSAttributedString *)attributedContentText;
-(void)dealloc;
-(id)initWithCoder:(id)arg1 ;
-(void)encodeWithCoder:(id)arg1 ;
-(id)init;
-(void)setAttributedTitle:(NSAttributedString *)arg1 ;
-(id)description;
-(void)setUserInfo:(NSDictionary *)arg1 ;
-(id)copyWithZone:(NSZone*)arg1 ;
-(NSDictionary *)userInfo;
-(NSAttributedString *)attributedTitle;
-(NSArray *)attachments;
-(void)setAttachments:(NSArray *)arg1 ;
-(void)setAttributedContentText:(NSAttributedString *)arg1 ;
-(id)_matchingDictionaryRepresentation;
@end

