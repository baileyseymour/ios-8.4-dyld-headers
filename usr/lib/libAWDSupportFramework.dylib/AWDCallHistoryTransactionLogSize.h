/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:39 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /usr/lib/libAWDSupportFramework.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <libAWDSupportFramework.dylib/libAWDSupportFramework.dylib-Structs.h>
#import <ProtocolBuffer/PBCodable.h>
#import <libAWDSupportFramework.dylib/NSCopying.h>

@interface AWDCallHistoryTransactionLogSize : PBCodable <NSCopying> {

	unsigned long long _timestamp;
	unsigned _records;
	unsigned _sizeBytes;
	SCD_Struct_AW7 _has;

}

@property (assign,nonatomic) BOOL hasTimestamp; 
@property (assign,nonatomic) unsigned long long timestamp;              //@synthesize timestamp=_timestamp - In the implementation block
@property (assign,nonatomic) BOOL hasSizeBytes; 
@property (assign,nonatomic) unsigned sizeBytes;                        //@synthesize sizeBytes=_sizeBytes - In the implementation block
@property (assign,nonatomic) BOOL hasRecords; 
@property (assign,nonatomic) unsigned records;                          //@synthesize records=_records - In the implementation block
-(BOOL)isEqual:(id)arg1 ;
-(unsigned long long)hash;
-(id)description;
-(unsigned long long)timestamp;
-(void)setTimestamp:(unsigned long long)arg1 ;
-(id)copyWithZone:(NSZone*)arg1 ;
-(unsigned)records;
-(id)dictionaryRepresentation;
-(BOOL)hasTimestamp;
-(void)setRecords:(unsigned)arg1 ;
-(void)setHasTimestamp:(BOOL)arg1 ;
-(void)mergeFrom:(id)arg1 ;
-(BOOL)readFrom:(id)arg1 ;
-(void)writeTo:(id)arg1 ;
-(void)copyTo:(id)arg1 ;
-(void)setSizeBytes:(unsigned)arg1 ;
-(void)setHasSizeBytes:(BOOL)arg1 ;
-(BOOL)hasSizeBytes;
-(void)setHasRecords:(BOOL)arg1 ;
-(BOOL)hasRecords;
-(unsigned)sizeBytes;
@end

