/*
* This header is generated by classdump-dyld 0.7
* on Thursday, July 2, 2015 at 10:06:42 AM Eastern Daylight Time
* Operating System: Version 8.4 (Build 12H143)
* Image Source: /usr/lib/libAWDSupportFramework.dylib
* classdump-dyld is licensed under GPLv3, Copyright © 2013 by Elias Limneos.
*/

#import <libAWDSupportFramework.dylib/libAWDSupportFramework.dylib-Structs.h>
#import <ProtocolBuffer/PBCodable.h>
#import <libAWDSupportFramework.dylib/NSCopying.h>

@class NSData;

@interface AWDWifiMostUsedNetworks : PBCodable <NSCopying> {

	double _timeUsed;
	unsigned _apOUI;
	NSData* _apOui;
	unsigned _securityType;
	unsigned _timeUsedMinutes;
	SCD_Struct_AW4 _has;

}

@property (assign,nonatomic) BOOL hasApOUI; 
@property (assign,nonatomic) unsigned apOUI;                        //@synthesize apOUI=_apOUI - In the implementation block
@property (assign,nonatomic) BOOL hasSecurityType; 
@property (assign,nonatomic) unsigned securityType;                 //@synthesize securityType=_securityType - In the implementation block
@property (assign,nonatomic) BOOL hasTimeUsed; 
@property (assign,nonatomic) double timeUsed;                       //@synthesize timeUsed=_timeUsed - In the implementation block
@property (nonatomic,readonly) BOOL hasApOui; 
@property (nonatomic,retain) NSData * apOui;                        //@synthesize apOui=_apOui - In the implementation block
@property (assign,nonatomic) BOOL hasTimeUsedMinutes; 
@property (assign,nonatomic) unsigned timeUsedMinutes;              //@synthesize timeUsedMinutes=_timeUsedMinutes - In the implementation block
-(void)dealloc;
-(BOOL)isEqual:(id)arg1 ;
-(unsigned long long)hash;
-(id)description;
-(id)copyWithZone:(NSZone*)arg1 ;
-(id)dictionaryRepresentation;
-(void)mergeFrom:(id)arg1 ;
-(BOOL)readFrom:(id)arg1 ;
-(void)writeTo:(id)arg1 ;
-(void)copyTo:(id)arg1 ;
-(void)setApOui:(NSData *)arg1 ;
-(void)setApOUI:(unsigned)arg1 ;
-(void)setHasApOUI:(BOOL)arg1 ;
-(BOOL)hasApOUI;
-(void)setSecurityType:(unsigned)arg1 ;
-(void)setHasSecurityType:(BOOL)arg1 ;
-(BOOL)hasSecurityType;
-(void)setTimeUsed:(double)arg1 ;
-(void)setHasTimeUsed:(BOOL)arg1 ;
-(BOOL)hasTimeUsed;
-(BOOL)hasApOui;
-(void)setTimeUsedMinutes:(unsigned)arg1 ;
-(void)setHasTimeUsedMinutes:(BOOL)arg1 ;
-(BOOL)hasTimeUsedMinutes;
-(unsigned)apOUI;
-(unsigned)securityType;
-(double)timeUsed;
-(NSData *)apOui;
-(unsigned)timeUsedMinutes;
@end

